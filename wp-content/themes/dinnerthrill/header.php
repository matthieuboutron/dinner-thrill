<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php
		global $page, $paged;
		wp_title( '|', true, 'right' );
		bloginfo( 'name' );
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?></title>
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_directory' ); ?>/bootstrap.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_directory' ); ?>/glDatePicker.css" />
	<?php wp_head(); ?>
        
    <style type="text/css" media="screen">
    html {
            margin-top: 28px !important;
    }
    * html body {
            margin-top: 28px !important;
    }
    </style>
</head>
<body <?php body_class(); ?>>
<?php
if( ! is_user_logged_in()): ?>
<script type="text/javascript">
    window.fbAsyncInit = function(){
        FB.init({appId:'126328197398781', status:true, cookie:true, xfbml:true, oauth:true});
    };
</script>
<div id="fb-root"></div>
<script type="text/javascript">
    (function() {
    var e = document.createElement('script');
    e.type = 'text/javascript';
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    e.async = true;
    document.getElementById('fb-root').appendChild(e);
    }());
</script>

 <?php endif; ?>     

<div id="main-container">

    <div id="header-container">
            <div id="header">
                    <div id="header-content">
                            <a id="header-logo" href="<?php bloginfo('url'); ?>" title="">
                                    <img src="<?php bloginfo('template_directory'); ?>/img/dinnerthrill-logo.png" alt="<?php _e('Dinner Thrill - to Homepage', 'dinnerthrill'); ?>" />
                            </a>
                            <img id="header-logo-backdrop" src="<?php bloginfo('template_directory'); ?>/img/logo-backdrop.png" alt="" />

                            <div id="header-menu">
                              <?php if(is_user_logged_in()): ?>  
                              <ul>
                                <li class="invite"><a href="#_" class="yellowarrow">Invite friends and get <strong>$10</strong></a></li>
                                <li><a href="/my-account/"><?php _e("My Account"); ?></a></li>
                                <li>|</li>
                                <li><a href="<?php echo wp_logout_url(home_url()); ?> "><?php _e("Sign Out"); ?></a></li>
                              </ul>
                              <?php endif; ?>   
                            </div>

                            <?php if( ! is_user_logged_in()): ?>
                            <div id="header-signin">
                              <div class="normal" id="sign-in-text-div">
                                <ul>
                                  <li>Already a member?</li>
                                  <li><a href="#_">Member Sign in</a></li>
                                </ul>
                              </div>
                              <div class="over" id="sign-in-form-div" style="display:none">
                                <div id="get-signin-form-container">
                                  <form id="get-signin-form" action="" method="POST">
                                    <input type="hidden" value="en" name="user_lang" id="user_lang" />
                                    <table border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <input type="hidden" id="security-login" name="security" value="<?php print wp_create_nonce( 'dt-ajax-form-login' );?>" />
                                        <td width="150"><div class="field-wrap email-field">
                                            <input type="text" id="user_login_email" name="user_login_email" maxlength="200" class="login-mandatory-field is-email" />
                                            <label for="user_login_email">Email</label>
                                          </div></td>
                                        <td width="130"><div class="field-wrap name-field">
                                            <input type="password" id="user_login_password" name="user_login_password" maxlength="200" class="login-mandatory-field" />
                                            <label for="user_login_password">Password</label>
                                          </div></td>
                                        <td width="70"><div class="submit-wrap signin-form-submit-field">
                                            <input type="submit" id="submit_login" name="submit_login" value="go" />
                                          </div></td>
                                        <td width="20" class="or">or</td>
                                        <td width="155"><div class="fb-login-mark" scope="email"><img src="<?php bloginfo('template_directory'); ?>/img/btn-connect-fb.png" width="213" height="28" alt="Connect to Facebook" /></div></td>
                                      </tr>
                                    </table>
                                  </form>
                                </div>
                              </div>
                            </div> 
                            <?php endif; ?>   
                    </div>

            </div>
    </div><!-- #header -->
