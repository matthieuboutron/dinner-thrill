<?php
// Template Name: Homepage
get_header(); 

if ( is_user_logged_in()) {
    include "template_home_logged.php";
}
else {
    include "template_home_subscribe.php";
}

get_footer();