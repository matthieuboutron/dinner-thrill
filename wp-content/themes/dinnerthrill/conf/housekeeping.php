<?php
// REMOVE THE USELESS DASHBOARD WIDGETS
function example_remove_dashboard_widgets() {
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
}
// Removes the "comment" column for pages
function custom_pages_columns($defaults) {
  unset($defaults['comments']);
  return $defaults;
}
add_filter('manage_pages_columns', 'custom_pages_columns');

// STRIP THE TEXT EDITOR OF ALL UNNECESSARY BUTTONS
function custom_options( $opt ) {
	//format drop down list
	$opt['theme_advanced_blockformats'] = 'h3,h4';
	$opt['theme_advanced_disable'] = 'justifycenter,justifyfull,justifyleft,justifyright,blockquote,underline,strikethrough,indent,help,fontselect,forecolor,backcolor,forecolorpicker,spellchecker,fullscreen';
	$opt['style_formats'] = "[{
        title: 'FAQ Block',
        block: 'div',
        classes: 'faq-question',
        wrapper: true
    	}]";
  $opt['theme_advanced_buttons1'] = 'bold,italic,separator,bullist,separator,link,unlink,separator,styleselect';
  $opt['theme_advanced_buttons2'] = 'formatselect,removeformat,charmap,undo,redo';
	return $opt;
}
add_filter('tiny_mce_before_init', 'custom_options');
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets' );

// Cleaning up wp_head
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');