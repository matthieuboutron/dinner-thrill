<?php
add_action( 'admin_head', 'cpt_icons' );
function cpt_icons() {
  global $user_login; ?>
    <style type="text/css" media="screen">
        #header-logo { 
            display: none;
        }
        #menu-posts {
          display: none;
        }
        <?php 
        // Hide items from user
        if($user_login != 'admin') : ?>
        #menu-posts, #menu-appearance, #menu-plugins, #menu-users, #menu-links, #menu-tools, #menu-settings, #menu-comments, #menu-media, #wp-admin-bar-wp-logo, #wp-admin-bar-comments, #wp-admin-bar-updates {
          display: none;
        }
        <?php endif; ?>
        #wphead h1 {
        }
        #wphead h1 a {
          display: block
        }
        /* ADMIN COLUMNS WIDTH */
        .column-product_image, .column-product_number {
          
        }
        .type-products .column-title {
          
        }

        /* Electrolyte Post Type */
        #menu-posts-electrolytic .wp-menu-image {
            height: 16px !important;
            margin: 7px 5px 7px 7px;
            width: 16px !important;
            background: url(<?php bloginfo('template_url') ?>/images/admin/icons.png) no-repeat -240px -24px !important;
        }
        #menu-posts-electrolytic:hover .wp-menu-image, #menu-posts-electrolytic.wp-has-current-submenu .wp-menu-image {
            background-position: -240px 0 !important;
        }
        .icon32-posts-electrolytic {
          width: 16px;
          height: 16px;
          margin: 17px 10px 5px 0;
          background: url(<?php bloginfo('template_url') ?>/images/admin/icons.png) no-repeat -240px 0 !important;
        }

        /* User Message */
        #toplevel_page_user_messages .wp-menu-image {
            height: 16px !important;
            margin: 7px 5px 7px 7px;
            width: 16px !important;
            background: url(<?php bloginfo('template_url') ?>/images/admin/icons.png) no-repeat -60px -24px !important;
        }
        #toplevel_page_user_messages .wp-menu-image a {
            display: none;
        }
        #toplevel_page_user_messages:hover .wp-menu-image, #toplevel_page_user_messages.wp-has-current-submenu .wp-menu-image {
            background-position: -60px 0 !important;
        }
        #icon-user-messages {
          width: 16px;
          height: 16px;
          margin: 17px 10px 5px 0;
          background: url(<?php bloginfo('template_url') ?>/images/admin/icons.png) no-repeat -60px 0 !important;
        }

        .column-user-name, .column-user-email, .column-user-subject {
          width: 15%;
        }
        .column-user-message {
          width: 60%;
        }
    </style>
<?php
}

/* // CONTACT MESSAGE PAGE
add_action('admin_menu', 'register_user_messages_page');
function register_user_messages_page() {
	add_menu_page( __('Message posted from Surfox\'s website'), __('User Messages'), 'edit_themes', 'user_messages', 'user_messages_content', '', 41); 
}
function user_messages_content() {
	global $title, $wpdb;
	?>
  <div class="wrap">
    <div id="icon-user-messages" class="icon32 icon32-user-messages">
      <br>
    </div>
    <h2>Messages from the users</h2>
    <table cellspacing="0" class="wp-list-table widefat fixed user-messages">
      <thead>
        <th id="user_name" class="column-user-name">Name</th>
        <th id="user_email" class="column-user-email">Email</th>
        <th id="user_subject" class="column-user-subject">Subject</th>
        <th id="user_message" class="column-user-message">Message content</th>
      </thead>
      <tbody id="the-list">
        <?php $query_messages = $wpdb->get_results("SELECT * FROM wp_contact ORDER BY id DESC LIMIT 0, 50");
        foreach($query_messages as $message) : ?>
        
          <tr>
            <td class="column-user-name"><strong><?php echo $message->first_name; ?> <?php echo $message->last_name; ?></strong><div class="more-info" style="display: none;">Language: <?php echo $message->user_lang; ?>, user IP: <?php echo $message->user_ip; ?></div></td>
            <td class="column-user-email"><a href="mailto:<?php echo $message->user_email; ?>"><?php echo $message->user_email; ?></a></td>
            <td class="column-user-subject"><?php echo $message->user_subject; ?></td>
            <td class="column-user-message"><?php echo $message->user_message; ?></td>
          </tr>
          
        <?php endforeach; ?> 
      </tbody>
      <tfoot>        
        <th id="user_name" class="column-user-name">Name</th>
        <th id="user_email" class="column-user-email">Email</th>
        <th id="user_subject" class="column-user-subject">Subject</th>
        <th id="user_message" class="column-user-message">Message content</th>
      </tfoot>
    </table>
  </div>
  <script type="text/javascript" charset="utf-8">
    jQuery(function() {
      jQuery('.column-user-name').hover(function() {
        jQuery(this).find('.more-info').stop(true, true).slideToggle('150');
      }, function() {
        jQuery(this).find('.more-info').stop(true, true).slideToggle('150');
      });
    });
  </script>
<?php 
}
*/

// PRODUCTS POST TYPE ADMIN COLUMNS
// Adding the columns
/*
add_filter( 'manage_edit-products_columns', 'set_edit_products_columns', 10, 1 );
add_filter( 'manage_edit-electrolytic_columns', 'set_edit_products_columns', 10, 1 );
add_filter( 'manage_edit-accessories_columns', 'set_edit_products_columns', 10, 1 );
function set_edit_products_columns( $columns ) {
  //$column_store = array( 'store_logo' => 'Company Logo', 'store_url' => 'Website' );
  //$columns = array_slice( $columns, 0, 1, true ) + $column_store + array_slice( $columns, 1, NULL, true );
  //return $columns;
  return array(
        'cb' => '<input type="checkbox" />',
        'product_image' => __('Product Image', 'surfox'),
        'title' => __('Title'),
        'product_trade_name' => __('Trade Name', 'surfox'),
        'product_number' => __('Product Number', 'surfox'),
    );
}
// Adding the actions
add_action( 'manage_pages_custom_column', 'set_edit_products_actions', 10, 1);
function set_edit_products_actions( $column ) {
  global $post;
  switch ( $column ) {
    case 'product_image':
      $logo = get_post_meta($post->ID, 'store_logo', 1);
      $lang = qtrans_getLanguage();
      if($lang == 'de' or $lang == 'ge') :
        echo wp_get_attachment_image(get_translated_meta('image_url'), array(100, 140), false); 
      elseif(get_translated_meta('image_url') != '') : ?>
        <img src="<?php echo get_bloginfo('template_directory') . '/images/ws_products/' . get_translated_meta('image_url'); ?>" />
      <?php endif;
      break;
    case 'product_trade_name':
      echo get_translated_meta('trade_name', true);
      break;
    case 'product_number':
      echo get_post_meta($post->ID, 'product_number', true);
      break;
  }
} */