<?php
// Template Name: Contact Us
get_header(); ?>

<?php get_template_part('part-section-title'); ?>

<div class="inside-pane">
	<?php get_sidebar(); ?>

	<div id="contactus-section-content" class="content-pane">

		<div id="contactus-form-container">

				<div id="response">
          <div class="is-response-code"></div>
          <div class="is-error-code is-missing-field"><?php _e('Please fill in all the fields!', 'dinnerthrill'); ?></div>
          <div class="is-error-code is-invalid-email"><?php _e('Check for spelling errors in your email address!', 'dinnerthrill') ?></div>
          <div class="is-error-code is-success"><?php _e('Your message has been sent successfully.', 'dinnerthrill') ?></div>
        </div>

        <form id="contact-us" action="" method="POST">

          <input type="hidden" value="<?php echo ICL_LANGUAGE_CODE; ?>" name="user_lang" id="user_lang" />
          <input type="hidden" value="<?php echo $_SERVER['SERVER_ADDR']; ?>" name="user_ip" id="user_ip" />

          <div class="form-part1">
	          <div class="field-wrap user-name-field">
	            <input type="text" id="user_name" name="user_name" maxlength="200" class="mandatory-field" />
	            <label for="user_name"><?php _e('Name', 'dinnerthrill'); ?></label>
	            <div class="error-sign"></div>
	          </div>

	          <div class="field-wrap email-field">
	            <input type="text" id="user_email" name="user_email" maxlength="200" class="mandatory-field is-email" />
	            <label for="user_email"><?php _e('Email', 'dinnerthrill'); ?></label>
	            <div class="error-sign"></div>
	          </div>

	          <div class="field-wrap subject-field">
	          	<div class="dropdown-selector" id="contact-subject" data-choices="List item1,List item2,List item3"></div>
	            <label for="message_subject"><?php _e('What can we help you with?', 'dinnerthrill'); ?></label>
	            <input type="hidden" id="message_subject" name="message_subject" value="null" />
	            <div class="error-sign"></div>
	          </div>
          </div>

          <div class="form-part2">
          	 <div class="field-wrap message-field">
	            <label for="message_content"><?php _e('Type your message here', 'dinnerthrill'); ?></label>
	            <textarea type="text" id="message_content" name="message_content" class="mandatory-field is-message" /></textarea>
	            <div class="error-sign"></div>
	          </div>
          </div>

          <input type="submit" id="submit_message" name="submit_message" value="<?php _e('Send', 'dinnerthrill') ?> &rarr;" />

        </form>

		</div>

	</div>

</div>

<?php get_footer(); ?>
