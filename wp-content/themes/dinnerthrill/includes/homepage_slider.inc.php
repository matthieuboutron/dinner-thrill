<?php

//_______ Homepage slider image
add_theme_support( 'post-thumbnails' );
add_image_size( 'homepage_slider-image', 960, 400, true );


//_______ Restaurant post type creation
add_action( 'init', 'create_homepage_slider_type' );

function create_homepage_slider_type() {

    $homepageSliderType = array(
        'labels' => array(
                            'name' => __( 'Homepage Sliders' ),
                            'singular_name' => __( 'Homepage Slider' ),
                            'menu_name' => __( 'Homepage Sliders' ),
                            'add_new' => __( 'Add New Homepage Slider' ),
                            'update_item' => __( 'Update Homepage Slider' ),
                            'add_new_item' => __( 'Add New Homepage Slider' ),
                             'new_item_name' => __( 'New Homepage Slider' )),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        "has_archive" => true,
        "supports" => array('title',
                            'editor', 
                            'thumbnail')
    );
    register_post_type( 'homepage_slider' , $homepageSliderType );

}


add_action( 'load-post.php', 'homepage_slider_restaurant_meta_setup' );
add_action( 'load-post-new.php', 'homepage_slider_restaurant_meta_setup' );
/* Create one or more meta boxes to be displayed on the post editor screen. */
function homepage_slider_restaurant_meta() {
  add_meta_box(
    'homepage_slider_restaurant',               // Unique ID
    esc_html__( 'Restaurant', 'dinnerthrill' ), // Title
    'homepage_slider_restaurant_meta_box',      // Callback function
    'homepage_slider',                          // Admin page (or post type)
    'side',                                     // Context
    'default'                                   // Priority
  );
}
/* Display the post meta box. */
function homepage_slider_restaurant_meta_box( $object, $box ) { ?>
  <?php global $wpdb; ?>
  <?php wp_nonce_field( basename( __FILE__ ), 'homepage_slider_restaurant_nonce' ); ?>
  <?php $currentValue = get_post_meta( $object->ID, 'homepage_slider_restaurant', true ); ?>
  <?php $restos = $wpdb->get_results("select * from $wpdb->posts where post_type = 'restaurant' and post_status = 'publish' order by post_title"); ?>
  <p>
    <label for="homepage_slider_restaurant"><?php _e( "This slide is related to this restaurant", 'dinnerthrill' ); ?></label>
    <br />
    <select name="homepage_slider_restaurant" id="homepage_slider_restaurant">
      <option value="" selected></option>
      <?php foreach ($restos as $resto): ?>
        <option value="<?php echo $resto->ID; ?>" <?php echo $currentValue==$resto->ID?'selected':''; ?>><?php echo $resto->post_title; ?></option>
      <?php endforeach; ?>
    </select>
  </p>
<?php }
/* Meta box setup function. */
function homepage_slider_restaurant_meta_setup() {
  /* Add meta boxes on the 'add_meta_boxes' hook. */
  add_action( 'add_meta_boxes', 'homepage_slider_restaurant_meta' );
  /* Save post meta on the 'save_post' hook. */
  add_action( 'save_post', 'homepage_slider_restaurant_save', 10, 2 );
}
/* Save the meta box's post metadata. */
function homepage_slider_restaurant_save( $post_id, $post ) {
  /* Verify the nonce before proceeding. */
  if ( !isset( $_POST['homepage_slider_restaurant_nonce'] ) || !wp_verify_nonce( $_POST['homepage_slider_restaurant_nonce'], basename( __FILE__ ) ) )
    return $post_id;
  /* Get the post type object. */
  $post_type = get_post_type_object( $post->post_type );
  /* Check if the current user has permission to edit the post. */
  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;
  /* Get the posted data and sanitize it for use as an HTML class. */
  $new_meta_value = ( isset( $_POST['homepage_slider_restaurant'] ) ? sanitize_html_class( $_POST['homepage_slider_restaurant'] ) : '' );
  /* Get the meta key. */
  $meta_key = 'homepage_slider_restaurant';
  /* Get the meta value of the custom field key. */
  $meta_value = get_post_meta( $post_id, $meta_key, true );
  /* If a new meta value was added and there was no previous value, add it. */
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, $meta_key, $new_meta_value, true );
  /* If the new meta value does not match the old value, update it. */
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, $meta_key, $new_meta_value );
  /* If there is no new meta value but an old value exists, delete it. */
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, $meta_key, $meta_value );
}
/* Filter the post class hook with our custom post class function. */
add_filter( 'post_class', 'homepage_slider_restaurant' );
function homepage_slider_restaurant( $classes ) {
  /* Get the current post ID. */
  $post_id = get_the_ID();
  /* If we have a post ID, proceed. */
  if ( !empty( $post_id ) ) {
    /* Get the custom post class. */
    $post_class = get_post_meta( $post_id, 'homepage_slider_restaurant', true );
    /* If a post class was input, sanitize it and add it to the post class array. */
    if ( !empty( $post_class ) )
      $classes[] = sanitize_html_class( $post_class );
  }
  return $classes;
}

add_action( 'load-post.php', 'homepage_slider_date_meta_setup' );
add_action( 'load-post-new.php', 'homepage_slider_date_meta_setup' );
/* Create one or more meta boxes to be displayed on the post editor screen. */
function homepage_slider_date_meta() {
  add_meta_box(
    'homepage_slider_date',               // Unique ID
    esc_html__( 'Date', 'dinnerthrill' ), // Title
    'homepage_slider_date_meta_box',      // Callback function
    'homepage_slider',                    // Admin page (or post type)
    'side',                               // Context
    'default'                             // Priority
  );
}
/* Display the post meta box. */
function homepage_slider_date_meta_box( $object, $box ) { ?>
  <?php wp_nonce_field( basename( __FILE__ ), 'homepage_slider_date_nonce' ); ?>
  <?php $currentValue = get_post_meta( $object->ID, 'homepage_slider_date', true ); ?>
  <p>
    <label for="homepage_slider_date"><?php _e( "Date for this special offer (mm/dd/YYYY)", 'dinnerthrill' ); ?></label>
    <br />
    <input type="text" name="homepage_slider_date" id="homepage_slider_date" value="<?php echo $currentValue; ?>" />
  </p>
<?php }
/* Meta box setup function. */
function homepage_slider_date_meta_setup() {
  /* Add meta boxes on the 'add_meta_boxes' hook. */
  add_action( 'add_meta_boxes', 'homepage_slider_date_meta' );
  /* Save post meta on the 'save_post' hook. */
  add_action( 'save_post', 'homepage_slider_date_save', 10, 2 );
}
/* Save the meta box's post metadata. */
function homepage_slider_date_save( $post_id, $post ) {
  /* Verify the nonce before proceeding. */
  if ( !isset( $_POST['homepage_slider_date_nonce'] ) || !wp_verify_nonce( $_POST['homepage_slider_date_nonce'], basename( __FILE__ ) ) )
    return $post_id;
  /* Get the post type object. */
  $post_type = get_post_type_object( $post->post_type );
  /* Check if the current user has permission to edit the post. */
  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;
  /* Get the posted data and sanitize it for use as an HTML class. */
  $new_meta_value = ( isset( $_POST['homepage_slider_date'] ) ? $_POST['homepage_slider_date'] : '' );
  /* Get the meta key. */
  $meta_key = 'homepage_slider_date';
  /* Get the meta value of the custom field key. */
  $meta_value = get_post_meta( $post_id, $meta_key, true );
  /* If a new meta value was added and there was no previous value, add it. */
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, $meta_key, $new_meta_value, true );
  /* If the new meta value does not match the old value, update it. */
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, $meta_key, $new_meta_value );
  /* If there is no new meta value but an old value exists, delete it. */
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, $meta_key, $meta_value );
}
/* Filter the post class hook with our custom post class function. */
add_filter( 'post_class', 'homepage_slider_date' );
function homepage_slider_date( $classes ) {
  /* Get the current post ID. */
  $post_id = get_the_ID();
  /* If we have a post ID, proceed. */
  if ( !empty( $post_id ) ) {
    /* Get the custom post class. */
    $post_class = get_post_meta( $post_id, 'homepage_slider_date', true );
    /* If a post class was input, sanitize it and add it to the post class array. */
    if ( !empty( $post_class ) )
      $classes[] = sanitize_html_class( $post_class );
  }
  return $classes;
}
