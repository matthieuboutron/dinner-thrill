<?php

//_______ Restaurant image
add_theme_support( 'post-thumbnails' );
add_image_size( 'large-restaurant-image', 665, 400, true );
add_image_size( 'restaurant-thumbnail-image', 273, 197, true );


//_______ Restaurant post type creation
add_action( 'init', 'create_restaurant_type' );

function create_restaurant_type() {

    $restaurantType = array(
        'labels' => array(
                            'name' => __( 'Restaurants' ),
                            'singular_name' => __( 'Restaurant' ),
                            'menu_name' => __( 'Restaurants' ),
                            'add_new' => __( 'Add New Restaurant' ),
                            'update_item' => __( 'Update Restaurant' ),
                            'add_new_item' => __( 'Add New Restaurant' ),
                             'new_item_name' => __( 'New Restaurant' )),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        "has_archive" => true,
        "supports" => array('title',
                            'editor', 
                            'thumbnail',
                            'custom-fields')
    );
    register_post_type( 'restaurant' , $restaurantType );
    
    
    //we register the various taxonomy
    register_taxonomy(
      'neighborhood',
       "restaurant",
      array(
        'hierarchical' => false,
        'label' => __("Neighborhood"),
        'show_ui' => true,
        'rewrite' => true,
        'query_var' => true
      ));
    
    register_taxonomy(
      'cuisine',
       "restaurant",
      array(
        'hierarchical' => true,
        'label' => __("Cuisine"),
        'show_ui' => true,
        'rewrite' => true,
        'query_var' => true
      )); 
    
    register_taxonomy(
      'scene',
       "restaurant",
      array(
        'hierarchical' => true,
        'label' => __("Scene"),
        'show_ui' => true,
        'rewrite' => true,
        'query_var' => true
      )); 
    
    register_taxonomy(
      'cost',
       "restaurant",
      array(
        'hierarchical' => false,
        'label' => __("Cost"),
        'show_ui' => true,
        'rewrite' => true,
        'query_var' => true
      )); 

    register_taxonomy(
      'booking_time',
       "restaurant",
      array(
        'hierarchical' => true,
        'label' => __("Horaires"),
        'show_ui' => true,
        'rewrite' => true,
        'query_var' => true
      )); 

}


//___________ Taxonomy styling
function custom_taxonomy_style() {
      remove_meta_box('tagsdiv-neighborhood','restaurant','core');
      add_meta_box('mark_dropdown_neighborhood', __('Neighborhood'), 'neighborhood_to_dropdown', 'restaurant', 'side', 'low');

      remove_meta_box('tagsdiv-cost','restaurant','core');
      add_meta_box('mark_dropdown_cost', __('Cost'), 'cost_to_dropdown', 'restaurant', 'side', 'low');

      
}	
 
function add_theme_menus() {
	if ( ! is_admin() )
		return;
	add_action('admin_menu', 'custom_taxonomy_style');
        /* Use the save_post action to save new post data */
	add_action('save_post', 'saveDropDownFields');
	add_action('edit_post', 'saveDropDownFields');
}
 
add_theme_menus();


function neighborhood_to_dropdown($post) {
    taxonomy_to_dropdown($post, "neighborhood");
}

function cost_to_dropdown($post) {
    taxonomy_to_dropdown($post, "cost");
}


function saveDropDownFields( $post_id ) {
    if ( !isset( $_POST[ 'taxonomy_restaurant_wpnonce' ] ) || !wp_verify_nonce( $_POST[ 'taxonomy_restaurant_wpnonce' ], 'taxonomy_restaurant' ) )
            return $post_id;
    if ( !current_user_can( 'edit_post', $post_id ) )
            return $post_id;

    // verify if this is an auto save routine. If it is our form has not been submitted, so we dont want to do anything
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
    return $post_id;

    $post = get_post($post_id);
    if (($post->post_type != 'revision') ) { 
       $tn = $_POST['neighborhood'];
       wp_set_object_terms( $post_id, $tn, 'neighborhood' );
       $tc = $_POST['cost'];
       wp_set_object_terms( $post_id, $tc, 'cost' );
    }
    return $post_id;
 
}

function taxonomy_to_dropdown($post, $taxo) {

        wp_nonce_field( 'taxonomy_restaurant', 'taxonomy_restaurant_wpnonce', false, true );
				
	$terms = get_terms($taxo, 'hide_empty=0'); 
 
?>
<select name='<? echo $taxo ?>' id='post_theme'>
	<!-- Display themes as options -->
    <?php 
        $names = wp_get_object_terms($post->ID, $taxo); 
        ?>
        <option class='theme-option' value='' 
        <?php if (!count($taxo)) echo "selected";?>>None</option>
        <?php
	foreach ($terms as $term) {
		if (!is_wp_error($names) && !empty($names) && !strcmp($term->slug, $names[0]->slug)) 
			echo "<option class='theme-option' value='" . $term->slug . "' selected>" . $term->name . "</option>\n"; 
		else
			echo "<option class='theme-option' value='" . $term->slug . "'>" . $term->name . "</option>\n"; 
	}
   ?>
</select>    
<?php
}



function getNeighborhoodForRestaurant($postId, $separator = ", ") {
    return getTermNameForRestaurant($postId, "neighborhood", $separator);
}

function getCostForRestaurant($postId, $separator = ", ") {
    return getTermNameForRestaurant($postId, "cost", $separator);
}

function getCuisineForRestaurant($postId, $separator = ", ") {
    return getTermNameForRestaurant($postId, "cuisine", $separator);
}

function getSceneForRestaurant($postId, $separator = ", ") {
    return getTermNameForRestaurant($postId, "scene", $separator);
}

function getTermNameForRestaurant($postId, $taxonomy, $separator = ", ") {
    $term_list = wp_get_post_terms($postId, $taxonomy, array("fields" => "names"));
    $value = '';
    foreach ($term_list as $term) {
        $value .= $term.$separator;
    }
    $value = trim($value, $separator);
    return $value;
}


function getTimesIn2Hours($numberOfTimes = 3) {
    global $post;
    $times = get_terms('booking_time');
    $realTimes = array();
    $returnTimes = array();
    foreach ($times as $t)
        $realTimes[] = strtotime(date("Y-m-d").' '.$t->name.':00');
    $in2Hours = time() + (2 * 60 * 60);
    sort($realTimes);
    foreach ($realTimes as $t) {
        if ($t >= $in2Hours)
            $returnTimes[] = date("H:i", $t);
        if (count($returnTimes) == 3)
            break;
    }
    return $returnTimes;
}