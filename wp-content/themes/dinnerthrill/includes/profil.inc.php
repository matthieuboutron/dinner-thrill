<?php

/*
 * Function used for the profil
 */


/* Load registration file. */
require_once( ABSPATH . WPINC . '/registration.php' );


//admin bar
add_filter('show_admin_bar', '__return_false'); 


function fb_footer(){
if( is_user_logged_in()):
	echo "<script type='text/javascript'>jQuery('#facebook_connect').hide(); </script>";
return;
endif;
?>
<script type="text/javascript">
    
      
jQuery('div.fb-login-mark').click(function(){
    
    FB.login(function(FB_response){
            if( FB_response.status === 'connected' ){
                    fb_intialize(FB_response);
            }
        },
        {scope: 'email, user_location'});
    
});   
    
    
    
jQuery('div.fb-login-button').click(function(){
    /*
    FB.login(function(FB_response){
            if( FB_response.status === 'connected' ){
                    fb_intialize(FB_response);
            }
        },
        {scope: 'email, user_location'});
        */
    FB.getLoginStatus(function(response) {
          console.log("FB response " + response.status);
          if (response.status === 'connected') {
            fb_intialize(response);
          }
        }); 
});

function fb_intialize(FB_response){
    FB.api( '/me', 'GET',
        {'fields':'id,email,location'},
        function(FB_userdata){
            jQuery.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: {"action": "fb_intialize", "FB_userdata": FB_userdata, "FB_response": FB_response},
                    success: function(user){
                        if( user.error ){
                                alert( user.error );
                        }
                        else if( user.loggedin ){
                                window.location.reload();
                        }
                    }
            });
        }
    );
};
</script>
<?php
}
add_action( 'wp_footer', 'fb_footer', 1 );


/**
 * Function called in ajax when the user loged in with FB
 */
function wp_ajax_fb_intialize(){
    @error_reporting( 0 ); // Don't break the JSON result
    header( 'Content-type: application/json' );

    if( !isset( $_REQUEST['FB_response'] ) || !isset( $_REQUEST['FB_userdata'] ))
        die( json_encode( array( 'error' => 'Authonication required.' )));

    $FB_response = $_REQUEST['FB_response'];
    $FB_userdata = $_REQUEST['FB_userdata'];
    $FB_userid = (int) $FB_userdata['id'];

    if( ! $FB_userid )
        die( json_encode( array( 'error' => 'Please connect your facebook account.' )));

    global $wpdb;
    $user_ID = $wpdb->get_var( "SELECT user_id FROM $wpdb->usermeta WHERE meta_key = '_fbid' AND meta_value = '$FB_userid'" );

    if( !$user_ID ){
        $user_email = $FB_userdata['email'];
        $user_ID = $wpdb->get_var( "SELECT ID FROM $wpdb->users WHERE user_email = '$user_email'" );

        if( !$user_ID ){
            if ( !get_option( 'users_can_register' ))
                die( json_encode( array( 'error' => 'Registration is not open at this time. Please come back later..' )));

            extract( $FB_userdata );

            $user_email = $email;
            if ( empty( $user_email ))
                die( json_encode( array( 'error' => 'Please re-connect your facebook account as we couldnt find your email address..' )));


            $user_pass = wp_generate_password( 12, false );
            $userdata = compact( 'user_login', 'user_email', 'user_pass', 'display_name' );

            $userdata = array(
                    'user_pass' => $user_pass,
                    'user_login' => $user_email,
                    'user_email' => $user_email,
                    'role' => get_option( 'default_role' ),
            );

            $user_ID = wp_insert_user($userdata);
            add_user_meta($user_ID, 'user_city', $user_city, true);
            add_user_meta($user_ID, 'user_lang', ICL_LANGUAGE_CODE, true);

            if ( is_wp_error( $user_ID ))
                die( json_encode( array( 'error' => $user_ID->get_error_message())));
            
            update_user_meta( $user_ID, '_fbid', (int) $id );
            //send a mail
            wp_new_user_notification($user_ID, $user_pass);
            
        }
        else{
            update_user_meta( $user_ID, '_fbid', (int) $FB_userdata['id'] );
        }
    }

    wp_set_auth_cookie( $user_ID, false, false );
    die( json_encode( array( 'loggedin' => true )));
}
add_action( 'wp_ajax_nopriv_fb_intialize', 'wp_ajax_fb_intialize' );





function createProfil(){
     
/* Check if users can register. */
$registration = get_option( 'users_can_register' );
 
 /* If user registered, input info. */
 if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'register' ) {
    $user_pass = wp_generate_password();
    $userdata = array(
            'first_name' => esc_attr( $_POST['user_name'] ),
            'nickname' => esc_attr( $_POST['user_name'] ),
            'user_login' => esc_attr( $_POST['email'] ),
            'role' => get_option( 'default_role' ),
    );


    if ( ! is_email($userdata['user_email'], true) )
            $error = __('You must enter a valid email address.');
    elseif ( email_exists($userdata['user_email']) )
            $error = __('Sorry, that email address is already used!');
    
    if ( ! $userdata['user_login'] )
            $error = __('A name is required for registration.');
    
    if ( empty ($_POST['city']) )
            $error = __('The city is required for registration.');

    else{
        $new_user = wp_insert_user( $userdata );

        //we set the meta fields
        update_usermeta( $new_user, 'city', esc_attr( $_POST['city']  ) );
        
        //if we create a profil
        if( ! is_user_logged_in() ) {
            //send a mail
            wp_new_user_notification($new_user, $user_pass);
            
            //log the user
            $login = wp_login( $_POST['user-email'], $user_pass );
            $login = wp_signon( array( 'user_login' => $_POST['email'], 'user_password' => $user_pass, 'remember' => $_POST['remember-me'] ), false );
        }
    }
  }
}


/* Add section to the edit user page in the admin*/
add_action( 'show_user_profile', 'mark_edit_user_section' );
add_action( 'edit_user_profile', 'mark_edit_user_section' );

/**
 * Adds an additional settings section on the edit user/profile page in the admin.  This section allows users to
 * select a city from a dropdown of terms from the city taxonomy. 
 *
 * @param object $user The user object currently being edited.
 */
function mark_edit_user_section( $user ) {

    getCityListDropDown(get_the_author_meta( 'user_city', $user->ID ) );
}



/* Update the profession terms when the edit user page is updated. */
add_action( 'personal_options_update', 'mark_save_user_terms' );
add_action( 'edit_user_profile_update', 'mark_save_user_terms' );

/**
 * Saves the term selected on the edit user/profile page in the admin. This function is triggered when the page
 * is updated.  We just grab the posted data and use wp_set_object_terms() to save it.
 *
 * @param int $user_id The ID of the user to save the terms for.
 */
function mark_save_user_terms( $user_id ) {

    
	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_usermeta( $user_id, 'user_city', $_POST['user_city'] );
}




//____________________________ USER CITY

function getCityListStyledDropDown($value = "") {
    $tax = get_taxonomy( 'city' );
    ?>

    <input type="text" id="user_city" value="<?php echo $value; ?>" name="user_city" class="mandatory-field" style="display:none;" />
      <label for="user_city"><?php  _e('Select a City', 'dinnerthrill'); ?></label>

        <?php

        /* Get the terms of the 'city' taxonomy. */
        $terms = get_terms( 'city', array( 'hide_empty' => false ) );
        $termsList = "";
        foreach($terms as $term){
            $termsList .= $term->name.", ";
        }
        $termsList = trim($termsList, ", ");
        ?>
      <div class="dropdown-selector" data-choices="<?php echo $termsList; ?>">
        </div>
      <div class="error-sign"></div>
<?php 
}
    
    


function getCityListDropDown($selectedValue = false) {
    
        $tax = get_taxonomy( 'city' );


	?>


	<table class="form-table">

		<tr>
			<th><label for="city"><?php _e( 'City' ); ?></label></th>

			<td>
                        <select name="city" id="select_city" class="postform">

                        <?php
                        
                        /* Get the terms of the 'profession' taxonomy. */
                        $terms = get_terms( 'city', array( 'hide_empty' => false ) );

                        foreach($terms as $term){
                            $selected = '';
                            if($selectedValue == $term->term_id)
                                $selected = ' selected="selected"';
                            
                                $term_name =$term->name;
                                echo '<option value="'.$term->term_id.'"'.$selected.'>'.$term_name.'</option>';
                        }
                       
		
			?>
                        </select>
                        </td>
		</tr>

	</table>
<?php 
}


register_taxonomy(
        'city',
        'user',
        array(
                'public' => true,
                'labels' => array(
                        'name' => __( 'Cities' ),
                        'singular_name' => __( 'City' ),
                        'menu_name' => __( 'Cities' )),
                'rewrite' => array(
                        'with_front' => true,
                        'slug' => 'author/cities' // Use 'author' (default WP user slug).
                ),
                'capabilities' => array(
                        'manage_terms' => 'edit_users', // Using 'edit_users' cap to keep this simple.
                        'edit_terms'   => 'edit_users',
                        'delete_terms' => 'edit_users',
                        'assign_terms' => 'read',
                ),
                'update_count_callback' => 'mark_upd_cities_count' // Use a custom function to update the count.
        )
);


/**
 * Function for updating the 'city' taxonomy count.  What this does is update the count of a specific term
 * by the number of users that have been given the term.  We're not doing any checks for users specifically here.
 * We're just updating the count with no specifics for simplicity.
 *
 * See the _update_post_term_count() function in WordPress for more info.
 *
 * @param array $terms List of Term taxonomy IDs
 * @param object $taxonomy Current taxonomy object of terms
 */
function mark_upd_cities_count( $terms, $taxonomy ) {
	global $wpdb;

	foreach ( (array) $terms as $term ) {

		$count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->term_relationships WHERE term_taxonomy_id = %d", $term ) );

		do_action( 'edit_term_taxonomy', $term, $taxonomy );
		$wpdb->update( $wpdb->term_taxonomy, compact( 'count' ), array( 'term_taxonomy_id' => $term ) );
		do_action( 'edited_term_taxonomy', $term, $taxonomy );
	}
}


add_filter( 'parent_file', 'fix_user_city_page' );
function fix_user_city_page( $parent_file = '' ) {
	global $pagenow;

	if ( ! empty( $_GET[ 'taxonomy' ] ) && $_GET[ 'taxonomy' ] == 'city' && $pagenow == 'edit-tags.php' ) {
		$parent_file = 'users.php';
	}

	return $parent_file;
}


/* Adds the taxonomy page in the admin. */
add_action( 'admin_menu', 'mark_add_city_admin_page' );

/**
 * Creates the admin page for the 'profession' taxonomy under the 'Users' menu.  It works the same as any
 * other taxonomy page in the admin.  However, this is kind of hacky and is meant as a quick solution.  When
 * clicking on the menu item in the admin, WordPress' menu system thinks you're viewing something under 'Posts'
 * instead of 'Users'.  We really need WP core support for this.
 */
function mark_add_city_admin_page() {

	$tax = get_taxonomy( 'city' );

	add_users_page(
		esc_attr( $tax->labels->menu_name ),
		esc_attr( $tax->labels->menu_name ),
		$tax->cap->manage_terms,
		'edit-tags.php?taxonomy=' . $tax->name
	);
}

/* Create custom columns for the manage profession page. */
add_filter( 'manage_edit-city_columns', 'mark_manage_city_user_column' );

/**
 * Unsets the 'posts' column and adds a 'users' column on the manage city admin page.
 *
 * @param array $columns An array of columns to be shown in the manage terms table.
 */
function mark_manage_city_user_column( $columns ) {

	unset( $columns['posts'] );

	$columns['users'] = __( 'Users' );

	return $columns;
}

/* Customize the output of the custom column on the manage city page. */
add_action( 'manage_city_custom_column', 'mark_manage_city_column', 10, 3 );

/**
 * Displays content for custom columns on the manage city page in the admin.
 *
 * @param string $display WP just passes an empty string here.
 * @param string $column The name of the custom column.
 * @param int $term_id The ID of the term being displayed in the table.
 */
function mark_manage_city_column( $display, $column, $term_id ) {

	if ( 'users' === $column ) {
		$term = get_term( $term_id, 'city' );
		echo $term->count;
	}
}