<?php
// Template Name: My Account - Settings



    /* Get user info. */
    global $current_user, $wp_roles;
    get_currentuserinfo();

    /* Load the registration file. */
    require_once( ABSPATH . WPINC . '/registration.php' );
    require_once( ABSPATH . 'wp-admin/includes' . '/template.php' ); // this is only for the selected() function

    /* If profile was saved, update profile. */
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) 
            && $_POST['action'] == 'update-user' ) {

            /* Update user password. */
            if ( !empty($_POST['user_password'] ) && !empty( $_POST['user_password2'] ) ) {
                    if ( $_POST['user_password'] == $_POST['user_password2'] )
                            wp_update_user( array( 'ID' => $current_user->id, 'user_pass' => esc_attr( $_POST['user_password'] ) ) );
                    else
                            $error = __('The passwords you entered do not match.  Your password was not updated.');
            }

            /* Update user information. */
 
            if ( ! empty( $_POST['email'] ) ) {
               update_usermeta( $current_user->id, 'user_email', esc_attr( $_POST['email'] ) );
               update_usermeta( $current_user->id, 'user_login', esc_attr( $_POST['email'] ) );
            }
            
            if ( ! empty( $_POST['user_city'] ) )
                update_usermeta( $current_user->id, 'user_city', esc_attr( $_POST['user_city'] ) );	
            if ( ! empty( $_POST['phone'] ) )
                update_usermeta( $current_user->id, 'phone', esc_attr( $_POST['phone'] ) );
            if ( ! empty( $_POST['zipcode'] ) )
                update_usermeta( $current_user->id, 'zipcode', esc_attr( $_POST['zipcode'] ) );
            if ( ! empty( $_POST['first_name'] ) )
                update_usermeta( $current_user->id, 'first_name', esc_attr( $_POST['first_name'] ) );
            if ( ! empty( $_POST['last_name'] ) )
                update_usermeta( $current_user->id, 'last_name', esc_attr( $_POST['last_name'] ) );

            /* Redirect so the page will show updated info. */
            if ( ! $error ) {
                    wp_redirect( get_permalink() );
                    exit;
            }
    }


get_header(); 

if ( is_user_logged_in()):
?>
<div id="main-pane">
    <div id="section-title">
      <h2><strong><?php _e("My Account."); ?></strong><br>
        <?php if( ! empty($current_user->first_name)) echo sprintf(__("Welcome %s !"), $current_user->first_name); ?></h2>
      <div id="section-description"> </div>
    </div>
    <div class="inside-pane">
      <div id="about-section-menu" class="navigation-menu">
        <ul id="menu-about-section" class="menu">
          <li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="">Current Bookings <span class="right-arrow">&rarr;</span></a></li>
          <li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="">Past Bookings <span class="right-arrow">&rarr;</span></a></li>
          <li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-15 current_page_item menu-item-17"><a href="">Settings <span class="right-arrow">&rarr;</span></a></li>
        </ul>
        <div class="dots-separator"></div>
      </div>
      <div id="account-section-content" class="content-pane">
    <form id="account-settings" action="<?php the_permalink(); ?>" method="POST">

            <div class="settings-section">
            <h3><?php _e("General Information"); ?></h3>

            <table width="400" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="150"><label for="first_name"><?php _e("Firstname"); ?></label></td>
                <td><div class="field-wrap first-name-field">
                    <input type="text" id="first_name" name="first_name" maxlength="200" class="mandatory-field" value="<?php echo $current_user->first_name; ?>" />
                    <div class="error-settings"></div>
                  </div></td>
              </tr>
              <tr>
                <td width="150"><label for="last_name"><?php _e("Lastname"); ?></label></td>
                <td><div class="field-wrap last-name-field">
                    <input type="text" id="last_name" name="last_name" maxlength="200" class="mandatory-field" value="<?php echo $current_user->last_name; ?>" />
                    <div class="error-settings"></div>
                  </div></td>
              </tr>
              <tr>
                <td width="150"> <label for="user_city"><?php _e("City"); ?></label></td>
                <td>
                    <div class="field-wrap city-field">
                      <?php
                          $value = get_the_author_meta( 'user_city', $current_user->id );
                          $tax = get_taxonomy( 'city' );
                        ?>

                            <input type="text" id="user_city" value="<?php echo $value; ?>" name="user_city" class="mandatory-field" />
                            <?php

                            /* Get the terms of the 'city' taxonomy. */
                            $terms = get_terms( 'city', array( 'hide_empty' => false ) );
                            $termsList = "";
                            foreach($terms as $term){
                                $termsList .= $term->name.", ";
                            }
                            $termsList = trim($termsList, ", ");
                            ?>
                          <div class="dropdown-selector" data-choices="<?php echo $termsList; ?>">
                          </div>
                          <div class="error-sign"></div>
                    </div>
                </td>
              </tr>
              <tr>
                <td width="150"> <label for="zipcode"><?php _e("Zip Code"); ?></label></td>
                <td> <div class="field-wrap zipcode-field">
                        <input type="text" id="zipcode" name="zipcode" maxlength="200" class="mandatory-field is-zipcode" value="<?php echo get_the_author_meta( 'zipcode', $current_user->id ) ?>" />

                        <div class="error-settings"></div>
                      </div>
                    </td>
              </tr>
              <tr>
                <td width="150"><label for="phone"><?php _e("Phone") ?></label></td>
                <td> <div class="field-wrap phone-field">
                                <input type="text" id="phone" name="phone" maxlength="200" class="mandatory-field is-phone" value="<?php echo get_the_author_meta( 'phone', $current_user->id ) ?>" />

                                <div class="error-settings"></div>
                              </div></td>
              </tr>
            </table>


            </div>
            <div class="settings-section">
            <h3><?php _e("Email") ?></h3>
            <table width="400" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="150"><label for="user_email"><?php _e("Email") ?></label></td>
                <td> <div class="field-wrap email-field">
                    <input type="text" id="user_email" name="user_email" maxlength="200" class="mandatory-field is-email" value="<?php echo $current_user->user_email; ?>" />

                    <div class="error-settings"></div>
                  </div></td>
              </tr>
            </table>



            </div>
            <div class="settings-section">
                <h3><?php _e("Password") ?></h3>
                <table width="400" border="0" cellspacing="0" cellpadding="0">
                  <tr>

                    <td width="150"><?php _e("New Password") ?></td>
                    <td><div class="field-wrap password-field">
                        <input type="password" id="user_password" name="user_password" maxlength="200" />
                        <label for="user_password"></label>
                        <div class="error-settings"></div>
                      </div></td>
                  </tr>
                  <tr>

                    <td width="150"><?php _e("Confirm New Password") ?></td>
                    <td><div class="field-wrap confirmpassword-field">
                        <input type="password" id="usewr_confirmpassword" name="user_password2" maxlength="200" />
                        <label for="user_password2"></label>
                        <div class="error-settings"></div>
                      </div></td>
                  </tr>
                </table>

            </div>
            <?php wp_nonce_field( 'update-user' ) ?>
            <input name="action" type="hidden" id="action" value="update-user" />

          <input type="submit" id="submit_message" name="submit_message" value="Update" />

        </form>        

      </div>
    </div>
    <div class="clear-it"></div>
  </div>
  <!-- #main-pane -->
<?php else: ?>
  <p class="warning">
        <?php _e('You must be logged in to edit your profile.'); ?>
</p><!-- .warning -->
  
<?php
endif;
get_footer();