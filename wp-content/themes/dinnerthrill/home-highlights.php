<?php $highlight = get_posts('post_type=homepage_slider&numberposts=1&orderby=rand'); ?>

    <?php foreach ($highlight as $h): ?>
      <?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($h->ID), 'homepage_slider-image'); ?>
      <div id="highlights" style="background:url(<?php echo $thumb[0]; ?>) no-repeat">
        <div class="bandeau">
          <h3><?php echo $h->post_title; ?></h3>
          <ul>
            <li><?php echo get_post_meta($h->ID, 'homepage_slider_date', 'true'); ?></li>
            <li><a href="<?php echo get_permalink(get_post_meta($h->ID, 'homepage_slider_restaurant', true)) ; ?>" class="yellowarrow">book today</a> </li>
          </ul>
        </div>
      </div>
    <?php endforeach; ?>
