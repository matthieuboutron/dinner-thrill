<div id="about-section-menu" class="navigation-menu">
  <?php
  // About Section menu
  $the_menu = wp_nav_menu(array('menu' => 'about-section', 'container' => false, 'echo' => false));
  $the_menu = str_replace('</a></li>', ' <span class="right-arrow">&rarr;</span></a></li>', $the_menu);

  echo $the_menu; ?>
  <div class="dots-separator"></div>
</div>