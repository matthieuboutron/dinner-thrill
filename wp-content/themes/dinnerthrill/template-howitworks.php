<?php
// Template Name: How It Works
get_header(); ?>

<?php get_template_part('part-section-title'); ?>

<div class="inside-pane">
	<?php get_sidebar(); ?>

	<div id="howitworks-section-content" class="content-pane">

		<div id="benefits-pane">
			<div id="for-restaurants" class="benefits-list">
				<div class="benefit-item benefit-1">
					<div class="number-blob">1</div>
					<h4><?php echo __('Browse', 'dinnerthrill'); ?></h4>
					<div class="benefit-description"><?php echo __('Dinner Thrill curates which restaurants are worth going to, and partners with them to give you insider pricing.', 'dinnerthrill'); ?></div>
				</div>

				<div class="benefit-item benefit-2">
					<div class="number-blob">2</div>
					<h4><?php echo __('Reserve', 'dinnerthrill'); ?></h4>
					<div class="benefit-description"><?php echo __('Members book tables through Dinner Thrill for $10 per reservation.', 'dinnerthrill'); ?></div>
				</div>

				<div class="benefit-item benefit-3">
					<div class="number-blob">3</div>
					<h4><?php echo __('Thrill', 'dinnerthrill'); ?></h4>
					<div class="benefit-description"><?php echo __('Our 30% discount is snuck onto your bill. No hassle or discomfort&mdash;the only surprise is the savings.', 'dinnerthrill'); ?></div>
				</div>			
			</div>

			<div id="for-customers" class="benefits-list">
				<div class="benefit-item benefit-1">
					<div class="number-blob">1</div>
					<h4><?php echo __('Browse', 'dinnerthrill'); ?></h4>
					<div class="benefit-description"><?php echo __('Dinner Thrill curates which restaurants are worth going to, and partners with them to give you insider pricing.', 'dinnerthrill'); ?></div>
				</div>

				<div class="benefit-item benefit-2">
					<div class="number-blob">2</div>
					<h4><?php echo __('Reserve', 'dinnerthrill'); ?></h4>
					<div class="benefit-description"><?php echo __('Members book tables through Dinner Thrill for $10 per reservation.', 'dinnerthrill'); ?></div>
				</div>

				<div class="benefit-item benefit-3">
					<div class="number-blob">3</div>
					<h4><?php echo __('Thrill', 'dinnerthrill'); ?></h4>
					<div class="benefit-description"><?php echo __('Our 30% discount is snuck onto your bill. No hassle or discomfort&mdash;the only surprise is the savings.', 'dinnerthrill'); ?></div>
				</div>				
			</div>
		</div>

		<div id="benefits-switcher">
			<div id="view-benefits-for-restaurants" class="benefit-switcher-button active"><?php _e('Benefits for restaurants', 'dinnerthrill'); ?></div>
			<div id="view-benefits-for-customers" class="benefit-switcher-button"><?php _e('Benefits for customers', 'dinnerthrill'); ?></div>

			<div id="benefits-long-description-restaurants" class="benefit-long-description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</div>
			<div id="benefits-long-description-customers" class="benefit-long-description">
				<p><strong><?php echo __('WHAT SMALL PRINT?', 'dinnerthrill'); ?></strong></p>
				<p><?php echo __('If something comes up, feel free to cancel your reservation up to two hours before the time your table’s been booked for.', 'dinnerthrill'); ?></p>
				<p><?php echo __('Don’t stress about spending “enough”&mdash;if the savings don’t cover the cost of the seating fee, the reservation is on us.', 'dinnerthrill'); ?></p>
			</div>
		</div>

	</div>

</div>

<?php get_footer(); ?>
