<div id="main-pane">
    <div id="home-pane">
      <div id="blackboard">
        <div id="the-blackboard-content">
          <div id="about-dinner-thrill-section">
            <div id="about-dinner-thrill-slider">
              <div class="slides_container">

                <div class="slide">
                  <h3><?php echo __('Make your dining reservations with Dinner Thrill and enjoy our insider pricing.', 'dinnerthrill'); ?></h3>
                  <div class="get-to-know">
                    <h4 class="first-part"><?php echo __('30%', 'dinnerthrill'); ?></h4>
                    <h4 class="second-part"><?php echo __('SAVINGS', 'dinnerthrill'); ?></h4>
                  </div>
                </div>

                <div class="slide">
                  <h3><?php echo __('It has never been that easy.', 'dinnerthrill'); ?></h3>
                  <div class="get-to-know">
                    <h4 class="second-part"><?php echo __('ONLY', 'dinnerthrill'); ?></h4>
                    <h4 class="first-part"><?php echo __('$ 10'); ?></h4>
                  </div>
                </div>

              </div>

            </div>

            <div id="about-section-links">
              <a href="<?php echo get_permalink(15); ?>" title=""><?php echo get_the_title(15); ?> &rarr;</a>
              <a href="<?php echo get_permalink(21); ?>" title=""><?php echo get_the_title(21); ?> &rarr;</a>
            </div>
          </div>

          <div id="curly-brace"></div>

          <div id="get-an-invite">
            <h3><?php echo __('Become a member for free', 'dinnerthrill'); ?></h3>

            <div id="get-invite-form-container">

              <form id="get-invite-form" action="" method="POST">

                <input type="hidden" value="<?php echo ICL_LANGUAGE_CODE; ?>" name="user_lang" id="user_lang" />

                 <div class="field-wrap email-field">
                  <input type="text" id="user_email" name="user_email" maxlength="200" class="mandatory-field is-email" />
                  <label for="user_email"><?php echo __('Enter Email Address', 'dinnerthrill'); ?></label>
                  <div class="error-sign"></div>
                </div>

               <div class="field-wrap name-field">
                  <input type="password" id="user_password" name="user_password" maxlength="200" class="mandatory-field" />
                  <label for="user_password"><?php echo __('Create a Password', 'dinnerthrill'); ?></label>
                  <div class="error-sign"></div>
                </div>

                <div class="field-wrap city-field">
                  <?php getCityListStyledDropDown(); ?>
                </div>
                
                <div id="response">
                  <div class="is-response-code"></div>
                  <div class="is-error-code is-missing-field"><?php echo __('Please fill in all the fields!', 'dinnerthrill'); ?></div>
                  <div class="is-error-code is-invalid-email"><?php echo __('Check for spelling errors in your email address!', 'dinnerthrill') ?></div>
                  <div class="is-error-code is-email-already-exists"><?php echo __('It looks like you\'ve already requested your invite!', 'dinnerthrill') ?></div>
                </div>

                <div class="submit-wrap invite-form-submit-field">
                  <div id="ajaxsave"><?php echo __('Loading...', 'dinnerthrill'); ?></div>
                  <input type="submit" id="submit_invite" name="submit_invite" value="<?php echo __('Join now', 'dinnerthrill') ?>" />
					<p><?php echo __('or', 'dinnerthrill'); ?></p>
                  <div class="fb-login-mark" id="facebook_connect" scope="email,user_location">
                  <img src="<?php bloginfo('template_directory'); ?>/img/btn-connect-fb.png" width="213" height="28" alt="Connect to Facebook" />
                    <?php // echo __('Login with Facebook', 'dinnerthrill'); ?>
                  </div>
                </div>

              </form>

            </div><!-- #get-invite-form-container -->
          </div><!-- #get-an-invite -->
        </div><!-- #the-blackboard-content -->
        <div id="the-blackboard-shadow"></div>
      </div><!-- #blackboard -->

    </div><!-- #home-pane -->
    <div class="clear-it"></div>
</div>