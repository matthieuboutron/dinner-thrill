<div id="main-pane">
<?php // Creates title section ?>
<div id="section-title">
  <h2><?php the_title(); ?></h2>

  <div id="section-description"><?php 
    echo apply_filters('the_content', get_post_meta($post->ID, 'short_description', true)); ?>
  </div>

</div>