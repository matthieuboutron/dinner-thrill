<?php

/**
 * Subscribes a new user from the homepage form
 * @author: Fred Mercy
 * @since 16 apr 2012
 */
function dt_subscribe_new_user() {
        $user_lang = $_POST['user_lang'];
        $user_city = $_POST['user_city'];
          
        $user_pass = esc_attr( $_POST['user_password']);
        $userdata = array(
                'user_pass' => $user_pass,
                'user_login' => esc_attr( $_POST['user_email'] ),
                'user_email' => esc_attr( $_POST['user_email'] ),
                'role' => get_option( 'default_role' ),
        );
        
	$user_id = wp_insert_user($userdata);
	add_user_meta($user_id, 'user_city', $user_city, true);
	add_user_meta($user_id, 'user_lang', $user_lang, true);

	if(is_wp_error($user_id)) {
		echo $user_id->get_error_code();
		//var_dump($user_id);
	} else {
            
            //if we create a profil
            if( ! is_user_logged_in() ) {
                //send a mail
                //wp_new_user_notification($new_user, $user_pass);

                //log the user
                wp_set_auth_cookie($user_id, $remember, $secure);
               // $login = wp_login( $_POST['user-email'], $user_pass );
               // $login = wp_signon( array( 'user_login' => $_POST['email'], 'user_password' => $user_pass, 'remember' => false ), false );
            }
            
	    echo 'is_ok';
	}
	die();
}
add_action('wp_ajax_dt_subscribe_new_user', 'dt_subscribe_new_user');
add_action('wp_ajax_nopriv_dt_subscribe_new_user', 'dt_subscribe_new_user');


/**
 * Function used to log in the user with ajax
 * @author Mark Baylet
 */
function dt_login_submit() {

    check_ajax_referer( 'dt-ajax-form-login', 'security' );

    $creds = array();
    $creds['user_login'] = $_POST['user_email'];
    $creds['user_password'] = $_POST['user_password'];

    $user = wp_signon( $creds, false );

    if ( is_wp_error( $user ) ) {
        echo strip_tags($user->get_error_message());
    }
    else
        echo 'is_ok';
    
    die();
}
add_action( 'wp_ajax_dt_login_submit', 'dt_login_submit' );
add_action( 'wp_ajax_nopriv_dt_login_submit', 'dt_login_submit' );


/**
 * Sends a contact email and stores it in the database
 * @author: Fred Mercy
 * @since 16 apr 2012
 */
function dt_contact_us() {
	global $wpdb;
  $user_lang = $_POST['user_lang'];
  $user_ip = $_POST['user_ip'];
  $user_name = $_POST['user_name'];
  $user_email = $_POST['user_email'];
  $message_subject = $_POST['message_subject'];
  $message_content = $_POST['message_content'];
  $date_sent = date('Y-m-d h:ia');

  if(!empty($user_email)) {
	  
	  $destination_email = get_option('contact_email_destination');
		// TEST MODE IF NEXT LINE IS UNCOMMENTED
		$destination_email = 'gcatusse@gmail.com';

		$subject = __('New message from Dinner Thrill contact form', 'dinnerthrill');
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $formatted =
    "
    <small>*** This message has been sent from Dinner Thrill's Contact Form. ***</small>
    <h2>Subject: " .$message_subject."</h2>
    <p>" .$message_content."</p>
    <h3>Person details: </h3>
    <ul>
	    <li>Name : ".$user_name."</li>
	    <li>Email : ". $user_email . "</li>
	    <li>Language : ". $user_lang . "</li>
	    <li>IP : ". $user_ip . "</li>
    </ul>";

		$contact_table = $wpdb->prefix . 'contact';
    $args = array( 
  		'user_lang' => $user_lang, 
  		'user_ip' => $user_ip,
  		'user_name' => $user_name,
  		'user_email' => $user_email,
  		'message_subject' => $message_subject,
  		'message_content' => $message_content,
  		'date_sent' => $date_sent,
    );
    // Inserts the message in the database
		$database_entry = $wpdb->insert($contact_table, $args);
		
		// Sends the email
		$email_sent = wp_mail($destination_email, $subject, $formatted, $headers);

  }

  if($email_sent) {
  	echo 'is_ok';
  } else {
  	echo 'is_failure';
  }

	die();
}
add_action('wp_ajax_dt_contact_us', 'dt_contact_us');
add_action('wp_ajax_nopriv_dt_contact_us', 'dt_contact_us');

/**
 * Sends a contact email and stores it in the database
 * @author: Matthieu Boutron
 * @since july 18th 2012
 */
function dt_our_partners() {
	global $wpdb;
  $user_lang = $_POST['user_lang'];
  $user_ip = $_POST['user_ip'];
  $user_name = $_POST['user_name'];
  $user_email = $_POST['user_email'];
  $user_restaurant = $_POST['user_restaurant'];
  $user_phone = $_POST['user_phone'];
  $date_sent = date('Y-m-d h:ia');

  if(!empty($user_email)) {
	  
	  $destination_email = get_option('contact_email_destination');
		// TEST MODE IF NEXT LINE IS UNCOMMENTED
		$destination_email = 'gcatusse@gmail.com';

		$subject = __('New message from Dinner Thrill our partners form', 'dinnerthrill');
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $formatted =
    "
    <small>*** This message has been sent from Dinner Thrill's Our Partners Form. ***</small>
    <h3>Person details: </h3>
    <ul>
	    <li>Name: ".$user_name."</li>
	    <li>Email: ". $user_email . "</li>
	    <li>Restaurant: ". $user_restaurant . "</li>
	    <li>Phone number: ". $user_phone . "</li>
	    <li>Language: ". $user_lang . "</li>
	    <li>IP: ". $user_ip . "</li>
    </ul>";

		$partners_table = $wpdb->prefix . 'partners';
    $args = array( 
  		'user_lang' => $user_lang, 
  		'user_ip' => $user_ip,
  		'user_name' => $user_name,
  		'user_email' => $user_email,
  		'user_restaurant' => $user_restaurant,
  		'user_phone' => $user_phone,
  		'date_sent' => $date_sent
    );
    // Inserts the message in the database
		$database_entry = $wpdb->insert($partners_table, $args);
		
		// Sends the email
		$email_sent = wp_mail($destination_email, $subject, $formatted, $headers);

  }

  if($email_sent) {
  	echo 'is_ok';
  } else {
  	echo 'is_failure';
  }

	die();
}
add_action('wp_ajax_dt_our_partners', 'dt_our_partners');
add_action('wp_ajax_nopriv_dt_our_partners', 'dt_our_partners');