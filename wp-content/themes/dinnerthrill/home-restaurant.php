<?php $neighborhood = get_the_terms($post->ID, 'neighborhood'); ?>
<?php $cuisine = get_the_terms($post->ID, 'cuisine'); ?>
      <div class="item">
          <div class="photo"><a href="<?php echo get_permalink($post->ID); ?>"><?php echo get_the_post_thumbnail($post->ID, 'restaurant-thumbnail-image'); ?></a></div>
        <div class="infos">
          <div class="left">
            <ul>
              <li><a href="<?php echo get_permalink($post->ID); ?>"><strong><?php echo $post->post_title; ?></strong></a></li>
              <li><?php if (is_array($neighborhood)) foreach($neighborhood as $n) echo $n->name; ?></li>
              <li><?php if (is_array($cuisine)) foreach($cuisine as $c) echo $c->name; ?></li>
            </ul>
          </div>
          <div class="right">
            <?php $times = getTimesIn2Hours(); ?>
            <ul>
              <?php foreach($times as $t): ?>
                <li><a href="<?php echo get_permalink($post->ID); ?>"><?php echo $t; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
          <div class="clear"></div>
        </div>
        <a href="<?php echo get_permalink($post->ID); ?>" class="yellowarrow"><?php echo __('See All Times', 'dinnerthrill'); ?></a>
      </div>
