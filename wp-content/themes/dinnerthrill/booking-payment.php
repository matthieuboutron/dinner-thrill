<?php if ($_POST): ?>
    <?php include_once("includes/DoDirectPayment.php"); ?>
<?php endif; ?>

<form method="POST">
    <input type="text" name="firstname" value="" />
    <input type="text" name="lastname" value="" />
    <select name="cardtype">
        <option value="visa">Visa</option>
        <option value="mastercard">Mastercard</option>
    </select>
    <input type="text" name="cardnumber" value="" />
    <input type="text" name="cardmonth" value="" />
    <input type="text" name="cardyear" value="" />
    <input type="text" name="cardcvv" value="" />
    <input type="text" name="address" value="" />
    <input type="text" name="city" value="" />
    <input type="text" name="state" value="" />
    <input type="text" name="zipcode" value="" />
    <input type="submit" value="<?php echo __('Payment', 'dinnerthrill'); ?>" />
</form>