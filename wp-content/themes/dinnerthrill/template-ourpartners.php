<?php
// Template Name: Our Partners
get_header(); ?>

<?php get_template_part('part-section-title'); ?>

<div class="inside-pane">
	<?php get_sidebar(); ?>

	<div id="ourpartners-section-content" class="content-pane">

            <div>
                <p>
                    <strong>Because nobody likes having a slow night.</strong><br />
                    Join Dinner Thrill to fill unused seating.
                </p>
                <p>
                    <strong>Pair up with us today.</strong><br />
                    Enter your information below
                </p>
            </div>
            
            <div id="contactus-form-container">
		<div id="response">
                    <div class="is-response-code"></div>
                    <div class="is-error-code is-missing-field"><?php _e('Please fill in all the fields!', 'dinnerthrill'); ?></div>
                    <div class="is-error-code is-invalid-email"><?php _e('Check for spelling errors in your email address!', 'dinnerthrill') ?></div>
                    <div class="is-error-code is-success"><?php _e('Your message has been sent successfully.', 'dinnerthrill') ?></div>
                </div>

        <form id="contact-us" action="" method="POST">

          <input type="hidden" value="<?php echo ICL_LANGUAGE_CODE; ?>" name="user_lang" id="user_lang" />
          <input type="hidden" value="<?php echo $_SERVER['SERVER_ADDR']; ?>" name="user_ip" id="user_ip" />

          <div class="form-part1">
	          <div class="field-wrap user-name-field">
	            <input type="text" id="user_name" name="user_name" maxlength="200" class="mandatory-field" />
	            <label for="user_name"><?php _e('Name', 'dinnerthrill'); ?></label>
	            <div class="error-sign"></div>
	          </div>

	          <div class="field-wrap email-field">
	            <input type="text" id="user_email" name="user_email" maxlength="200" class="mandatory-field is-email" />
	            <label for="user_email"><?php _e('Email', 'dinnerthrill'); ?></label>
	            <div class="error-sign"></div>
	          </div>
          </div>

          <div class="form-part2">
	          <div class="field-wrap user-name-field">
	            <input type="text" id="user_restaurant" name="user_restaurant" maxlength="255" class="mandatory-field" />
	            <label for="user_restaurant"><?php _e('Restaurant name', 'dinnerthrill'); ?></label>
	            <div class="error-sign"></div>
	          </div>

	          <div class="field-wrap user-name-field">
	            <input type="text" id="user_phone" name="user_phone" maxlength="50" class="mandatory-field" />
	            <label for="user_phone"><?php _e('Phone number', 'dinnerthrill'); ?></label>
	            <div class="error-sign"></div>
	          </div>
          </div>

          <input type="submit" id="submit_message" name="submit_message" value="<?php _e('Send', 'dinnerthrill') ?> &rarr;" />

        </form>

            </div>

	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; 
		endif; ?>
	</div>

</div>

<?php get_footer(); ?>
