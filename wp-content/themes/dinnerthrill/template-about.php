<?php
// Template Name: About Section
get_header(); ?>

<?php get_template_part('part-section-title'); ?>

<div class="inside-pane">
	<?php get_sidebar(); ?>

	<div id="about-section-content" class="content-pane">
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; 
		endif; ?>
	</div>

</div>

<?php get_footer(); ?>
