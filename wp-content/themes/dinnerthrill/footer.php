<div class="clear-it"></div>

</div><!-- #main-pane -->

<div id="footer-container">

  <div id="footer">
    <div id="dinnerthrill-copyright">© 2012 Dinner Thrill</div>
    <div id="legal"></div>
    <div id="footer-menu">
      <?php wp_nav_menu(array('menu' => 'footer', 'container' => false)); ?>
    </div>
    <div id="footer-social">
      <div class="social-title"><?php _e('Follow us',
              'dinnerthrill'); ?></div>
      <div class="social-facebook"><a href="<?php echo get_option('social_facebook'); ?>" title="<?php _e('on Facebook',
              'dinnerthrill'); ?>"><?php _e('on Facebook',
              'dinnerthrill'); ?></a></div>
      <div class="social-twitter"><a href="<?php echo get_option('social_twitter'); ?>" title="<?php _e('on Twitter',
              'dinnerthrill'); ?>"></a></div>
    </div>
  </div>

</div>

<div class="dropdown-backdrop"></div>

</div><!-- #main-container -->




<?php if(is_front_page()) : ?>
  <div class="background-element home-left"><img class="image" src="<?php bloginfo('template_directory') ?>/img/wallpapers/home-left.jpg" /></div>
  <div class="background-element home-right"><img class="image" src="<?php bloginfo('template_directory') ?>/img/wallpapers/home-right.jpg" /></div>
  <?php endif; ?>

<?php if(is_page(array('contact-us', 'first-things-first'))) : ?>
  <div class="background-element garlic-left"><img class="image" src="<?php bloginfo('template_directory') ?>/img/wallpapers/garlic-left.jpg" /></div>
  <div class="background-element garlic-right"><img class="image" src="<?php bloginfo('template_directory') ?>/img/wallpapers/garlic-right.jpg" /></div>
<?php endif; ?>

<script type="text/javascript">

  function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
  };

  $(function() {

    // TODO ScrollSpy that changes the menu upon scrolling
    // $(window).scroll(function() {
    //   var position = $(this).scrollTop();
    //   console.log(position);
    //   if(position > 56) {
    //     $('#header-container').addClass('fixed');
    //   } else {
    //     $('#header-container').removeClass('fixed');
    //   }
    // });
function isValidDate(date)
{
    var matches = /^(\d{4})-(\d{1,2})-(\d{1,2})$/.exec(date);
    if (matches == null) return false;
    var d = matches[3];
    var m = matches[2] - 1;
    var y = matches[1];
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}
function validateStep1()
{ 
  e=0;
  p =  parseInt($('#select_people').val());
  d =  isValidDate($('#select_date').val());
  t =  parseInt($('#select_meals').val());
  
  col="";
  if(isNaN(p) || p<1 || p>8)
    {col="#f44";e=1;}
  $('label[for="select_people"],#select_people').css('color',col);
  
  col="";
  if(!d)
    {col="#f44";e=1;}
  $('label[for="select_date"],#select_date').css('color',col);
  
  col="";
  if(isNaN(t))
    {col="#f44";e=1;}
  $('label[for="select_meals"],#select_meals').css('color',col);
  
  if(e)
    $('#errs').show();
  else $('#errs').hide();
  return e==0;
  
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function validateStep2Field(fname)
{
  v = $('#'+fname).val().replace(/\s/g,"");
  vn = parseInt(v);
  isnum = isNumber(v);
  switch(fname)
  {
  case "first_name":
  case "last_name":
    return v && v.length>1;
  case "phone_number":
    return v && v.length>=10;
  case "creditcard_number":
    return v && v.length==16;
  case "month_card":
    return v && vn>0 && vn<13;
  case "year_card":
    return v && vn>=12 && vn<20;
  case "ccv":
    return v && v.length==3 && isnum;
  case "address":
    return v && v.length>4;
  case "city":
    return v && v.length>1;
  case "prov":
    return v && v.length==2 && (v.toUpperCase() in {AB:true, BC:true,MB:true,NB:true,NL:true,NS:true,NT:true,NU:true,ON:true,PE:true,QC:true,SK:true,YT:true});
  case "postal_code":
    return v && v.length==6 && /^[a-z0-9A-Z]+$/.test(v);
  default:
    return false;
  }
}


function validateStep2()
{ 
  e=0;
  fields = Array('first_name','last_name','phone_number','creditcard_number','month_card','year_card','ccv','address','postal_code','prov','city');
  for(var i=0;i<fields.length;i++)
    {
      fname = fields[i];
      col="";
      if(!validateStep2Field(fname))
        {col="#f44";e=1;}
    $('label[for="'+fname+'"],#'+fname).animate({'color':col});
    }
  if(e)
    $('#errs').show();
  else $('#errs').hide();
  return e==0;
  
}


function updateContent(html,func)
{
  $('#mod-reservation').slideUp(500,function(){$('#mod-reservation').html(html).slideDown(500);func();});
}

function initstep2(){
  
      $('#show_special').click(function(){$('#special_request_tr').slideDown();});

      $('#reserv-step2 input').blur(function(){
        v=$(this).val();
        if(v  =="")
          $('label[for="'+$(this).attr('id')+'"]').show();
        else
          $('label[for="'+$(this).attr('id')+'"]').hide();
      });

      $('#reserv-step2 input').focus(function(){
        $('label[for="'+$(this).attr('id')+'"]').hide();
      });

      $('#step2-submit').click(function(){
        if(validateStep2())
          jQuery.post(
        '<?php echo admin_url('admin-ajax.php') ?>',
        $('#reserv-step2').serialize()+'&post_ID=<?php echo $post->ID; ?>&action=ajax-submit-step2',
        function( response ) {
          updateContent(response);
        }
      );
      });

      $('#reserv-step2 input').blur();
      $('#step2-submit-back').click(function(){
        jQuery.post(
        '<?php echo admin_url('admin-ajax.php') ?>',
        $('#reserv-step2').serialize()+'&post_ID=<?php echo $post->ID; ?>&action=ajax-submit-step2-back',
        function( response ) {
          updateContent(response,initstep1);
          //alert( print_r(response,true));
        }
      );
      });

    }

function initstep3()
{
      $('#step3-submit-back').click(function(){
        jQuery.post(
        '<?php echo admin_url('admin-ajax.php') ?>',
        $('#reserv-step3').serialize()+'&post_ID=<?php echo $post->ID; ?>&action=ajax-submit-step3-back',
        function( response ) {
          updateContent(response,initstep2);
          //alert( print_r(response,true));
        }
      );
      });
  
}
  
  function initstep1(){
      $('#step1-submit').click(function(){
        if(validateStep1())
          jQuery.post(
        '<?php echo admin_url('admin-ajax.php') ?>',
        $('#reserv-step1').serialize()+'&post_ID=<?php echo $post->ID; ?>&action=ajax-submit-step1',
        function( response ) {
          updateContent(response,initstep2);
          //alert( print_r(response,true));
        }
      );
      });

     $('#home-step1-submit').click(function(){
       // TODO JK + MB
       post_id = 9;// il faut mettre le post id du resto courant du bandeau
        if(validateStep1())
          jQuery.post(
        '<?php echo admin_url('admin-ajax.php') ?>',
        $('#reserv-step1').serialize()+'&redirect=1&post_ID='+post_id+'&action=ajax-submit-step1',
        function( response ) {
          //alert(response);
          window.location.href = response;//"/dinner-thrill/restaurant/thai-grill";
        }
      );
      });


      $('.field-wrap, #select_date').click(function(){
        $('.collapsable').stop().hide('slow');
        $('tr[rel="'+$(this).attr('rel')+'"]').stop().show('slow');
      });
 
      $('.time a').click(function(){
        $('#select_meals').val($(this).attr('rel'));
        $('label[for="select_meals"]').html($(this).html());
        $('.collapsable').stop().hide('slow');
      });

      $('.people a').click(function(){
        $('#select_people').val($(this).attr('rel'));
        $('label[for="select_people"]').html($(this).html());
        $('.collapsable').stop().hide('slow');
      });


<?php if(is_front_page()): ?>
        $('#select_date').glDatePicker({zIndex:999,top:"45px"});
<?php else: ?>
        $('#select_date').glDatePicker({onChange:function(){$('tr[rel="date-field"]').stop().hide('slow');},showAlways:true,placeTarget:"date-target",zIndex:999,cssName:'default',position:'static',top:45});
<?php endif; ?>
      // Displays / Hides the dropdown choices

      $('#dropdown-date').click(function(){$('#select_date').click();return false;});

    }

    <?php echo "initstep".($_GET['rhash']?"2":"1")."();";?>


<?php if(is_front_page()) : ?>

        $('#about-dinner-thrill-slider').slides({
          preload: true,
          preloadImage: '/img/loading.gif',
          generatePagination: false,
          generateNextPrev: true,
          effect: 'fade',
          fadeSpeed: 500,
          fadeEasing: 'easeInOutCubic'
        });

        // Initializes label placeholders
        $('#get-invite-form label').each(function() {
          var value = $(this).parent().find('input').val();
          if(value != '') {
            if($(this).parent().hasClass('city-field') == true) {
              $(this).animate({'left': '190px', 'opacity': '0.0' }, 300, 'easeOutBack');
            } else {
              $(this).animate({'left': '210px', 'opacity': '0.0' }, 300, 'easeOutBack');
            }
            // if($(this).parent().hasClass('city-field') == true) {
            //   $(this).fadeOut();
            // }
          }
          // else 
          // {
          //   $(this).show();
          // } 
        });
        // Hides label placeholder on focus
        $('#get-invite-form input').focus(function() {
          if($(this).is('#user_city')) {
            $(this).parent().find('label').animate({
              'left': '190px',
              'opacity': '0.0' }, 300, 'easeOutBack');
          } else {
            $(this).parent().find('label').animate({
              'left': '210px',
              'opacity': '0.0' }, 300, 'easeOutBack');
          }
      

        });
        // Show label placeholder on blur
        $('#get-invite-form input').blur(function() {
          if($(this).val() == '') {
            $(this).parent().find('label').animate({
              'left': '5px',
              'opacity': '1' }, 300, 'easeInBack');
          }
        });


        // Clears the error styling when user changes value
        $('#get-invite-form input').change(function() {
          $(this).parent().removeClass('has_error');
          $(this).parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
        });


        // Creates the user account on submit
        $('#get-invite-form').submit(function(e) {
          e.preventDefault();
          var error = 0;
          var error_code = {
            is_missing_field    : false,
            is_blacklisted      : false,
            is_invalid_email    : false
          }
          var user_password = $('#user_password').val();
          var user_email = $('#user_email').val();
          var user_city = $('#user_city').val();
          //var user_lang = $('#cc_user_lang').val();

          // Removes error messages (from previous attempts)
          $('.is-error-code').slideUp(200, 'easeOutCubic');

          $('.mandatory-field').each(function() {
            if($(this).val() == '') {
              $(this).parent().addClass('has_error');
              $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
              error++;
              error_code.is_missing_field = true;
            } else {
              if($(this).is('#user_email') && !isValidEmailAddress(user_email)) {
                $(this).parent().addClass('has_error');
                $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
                error++;
                error_code.is_invalid_email = true;
              } else {
                $(this).parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
                $(this).parent().removeClass('has_error');         
              }
            }
          }); 
      
          if(error > 0) {
            console.log('This should be displayed only once upon submit.');
            if(error_code.is_invalid_email) $('.is-invalid-email').slideDown(500, 'easeOutCubic');
            if(error_code.is_missing_field) $('.is-missing-field').slideDown(500, 'easeOutCubic');
            return false;
          } else {
            var data = {
              action: 'dt_subscribe_new_user',
              user_password: user_password,
              user_email: user_email,
              user_city: user_city
              //user_lang: user_lang,
            };
            $('#submit_invite').hide('slide', {direction: 'down'}, 300);
            $('#ajaxsave').show('slide', {direction: 'up'}, 300);
            $.post("<?php echo admin_url('admin-ajax.php'); ?>", data,
            function(response){
              $('#ajaxsave').hide('slide', {direction: 'down'}, 300);
              $('#submit_invite').show('slide', {direction: 'up'}, 300);
              $('.mandatory-field').removeClass('has_error');
              if(response == 'existing_user_login' || response == 'existing_user_email') 
                $('.is-email-already-exists').slideDown(500, 'easeOutCubic');
              if(response == 'is_ok')
                location.reload();
              //$('#get-invite-form-container').slideUp(500, 'easeOutCubic');
            });   
            return false;
          }
        });

<?php endif; ?>

<?php if(is_front_page()): ?>
      // **** exclude date field since it has it's own control
      $('.subject-field, .dropdown-selector').not('#dropdown-date').click(function() {
        var dropdownSelectorId = $(this).attr("id");
        $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
        $('#dropdown-choices_'+dropdownSelectorId).fadeIn(300, 'easeOutCubic');

      });
      $('.dropdown-backdrop').click(function() {
        $(this).fadeOut(300, 'easeOutCubic');
        $('.dropdown-choices').fadeOut(300, 'easeOutCubic');
      })

      // Displays / Hides the dropdown choices (**** exclude date field since it has it's own control)
      $('.dropdown-selector').not('#dropdown-date').click(function() {
        var dropdownSelector = $(this);
        var dropdownSelectorId = $(this).attr("id");
        if($('#dropdown-choices_'+dropdownSelectorId).length == 0) {
          $('#main-container').append('<div class="dropdown-choices" id="dropdown-choices_'+dropdownSelectorId+'"><ul class="dropdown-choices-list" id="dropdown-choices-list_'+dropdownSelectorId+'"></ul></div>');
        }
        var position = dropdownSelector.parent().offset();
        var item_height = dropdownSelector.parent().height() - 18;
        var item_width = dropdownSelector.parent().width();
        var choices = dropdownSelector.attr('data-choices');
        choices = choices.split(',');
      
        $('#dropdown-choices_'+dropdownSelectorId).css('top', position.top + item_height + 'px').css('left', position.left - (($('#dropdown-choices_'+dropdownSelectorId).width() - item_width) / 2) + 'px');
        var choice;
        $('#dropdown-choices-list_'+dropdownSelectorId).html('');
        for (choice in choices) {
          $('#dropdown-choices-list_'+dropdownSelectorId).append('<li>'+choices[choice]+'</li>');
        }
        $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
        $('#dropdown-choices_'+dropdownSelectorId).fadeIn(300, 'easeOutCubic');
        $('#dropdown-choices_'+dropdownSelectorId).find('li').click(function(e) {
          var the_choice = $(this).html();
          dropdownSelector.parent().find('input').val(the_choice);
          var the_label = dropdownSelector.parent().find('label');
          var original_left = the_label.css("left");
          the_label.animate({'left': '190px', 'opacity': '0.0' }, 300, 'easeOutBack', function(){the_label.html(the_choice).animate({'left': original_left, 'opacity': '1' }, 300, 'easeOutBack');});
          $('.dropdown-backdrop').click();
        });
      });
<?php endif; ?>

    $("#jqFilter").click(function(){
      $("#jqResults").slideUp('fast', function(){$(this).empty();});
      $.ajax({
        url:"<?php echo bloginfo('url') ?>/wp-admin/admin-ajax.php",
        type:'POST',
        data:'action=load_restaurants&neighborhood='+$("#select_neighborhood").val()+'&cuisine='+$("#select_cuisine").val()+'&scene='+$("#select_scene").val()+'&cost='+$("#select_cost").val(),
        success:function(results){
          $("#jqResults").slideUp('fast', function(){
            $("#jqResults").html(results).slideDown('fast', function(){$('html,body').animate({scrollTop: $("#filters").offset().top}, 'slow');});
          })
        }
      });
      return false;
    });
    $("#jqSeeAll").click(function(){
      $("#jqResults").slideUp('fast', function(){$(this).empty();});
      $.ajax({
        url:"<?php echo bloginfo('url') ?>/wp-admin/admin-ajax.php",
        type:'POST',
        data:'action=load_restaurants',
        success:function(results){
          $("#jqResults").slideUp('fast', function(){
            $("#jqResults").html(results).slideDown('fast', function(){$('html,body').animate({scrollTop: $("#filters").offset().top}, 'slow');});
          })
        }
      });
      return false;
    });
  
<?php if(is_page('single_restaurant')): ?>
        $('.dropdown-selector').click(function(){});
<?php endif; ?>
  
<?php if(is_page('first-things-first')) : ?>

        $('#benefits-pane').slideDown(500, 'easeInOutCubic');
        $('#for-restaurants .benefit-1').delay(500).animate({left: '0px'}, 300, 'easeOutCubic');
        $('#for-restaurants .benefit-2').delay(500).animate({left: '220px'}, 450, 'easeOutCubic');
        $('#for-restaurants .benefit-3').delay(500).animate({left: '440px'}, 800, 'easeOutCubic');
        $('#for-restaurants').addClass('active');
        $('#benefits-long-description-restaurants').slideDown(500, 'easeInOutCubic');

        $('#view-benefits-for-restaurants').click(function() {
          $(this).addClass('active');
          $('#view-benefits-for-customers').removeClass('active');

          $('#benefits-long-description-customers').slideUp(500, 'easeInOutCubic', function() {
            $('#benefits-long-description-restaurants').slideDown(500, 'easeInOutCubic');
          });

          $('#benefits-pane').slideDown(1000, 'easeInOutCubic');

          if($('#for-customers').hasClass('active') == true) {
            $('#for-customers .benefit-1').animate({top: '350px'}, 300, 'easeInCubic');
            $('#for-customers .benefit-2').animate({top: '350px'}, 450, 'easeInCubic');
            $('#for-customers .benefit-3').animate({top: '350px'}, 500, 'easeInCubic', function() {
              $('#for-customers .benefit-item').css('top', '0').css('left', '800px');
            });
            $('#for-customers').removeClass('active');
          }
          $('#for-restaurants .benefit-1').delay(500).animate({left: '0px'}, 300, 'easeOutCubic');
          $('#for-restaurants .benefit-2').delay(500).animate({left: '220px'}, 450, 'easeOutCubic');
          $('#for-restaurants .benefit-3').delay(500).animate({left: '440px'}, 800, 'easeOutCubic');
          $('#for-restaurants').addClass('active');

        });

        $('#view-benefits-for-customers').click(function() {
          $(this).addClass('active');
          $('#view-benefits-for-restaurants').removeClass('active');

          $('#benefits-long-description-restaurants').slideUp(500, 'easeInOutCubic', function() {
            $('#benefits-long-description-customers').slideDown(500, 'easeInOutCubic');
          });    

          $('#benefits-pane').slideDown(1000, 'easeInOutCubic');

          if($('#for-restaurants').hasClass('active') == true) {
            $('#for-restaurants .benefit-1').animate({top: '350px'}, 300, 'easeInCubic');
            $('#for-restaurants .benefit-2').animate({top: '350px'}, 450, 'easeInCubic');
            $('#for-restaurants .benefit-3').animate({top: '350px'}, 500, 'easeInCubic', function() {
              $('#for-restaurants .benefit-item').css('top', '0').css('left', '800px');
            });
            $('#for-restaurants').removeClass('active');
          }
          $('#for-customers .benefit-1').delay(500).animate({left: '0px'}, 300, 'easeOutCubic');
          $('#for-customers .benefit-2').delay(500).animate({left: '220px'}, 450, 'easeOutCubic');
          $('#for-customers .benefit-3').delay(500).animate({left: '440px'}, 800, 'easeOutCubic');
          $('#for-customers').addClass('active');
        });

<?php endif; ?>

<?php if(is_page('contact-us')) : ?>

        $('.dropdown-selector').click(function() {
          if($('.dropdown-choices').length == 0) {
            $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
          }
          var position = $(this).parent().offset();
          var item_height = $(this).parent().height() - 18;
          var item_width = $(this).parent().width();
          if($(this).is('.dropdown-selector') == true)
            var choices = $(this).attr('data-choices');
          else
            var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
          choices = choices.split(',');
      
          $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
          var choice;
          $('.dropdown-choices-list').html('');
          for (choice in choices) {
            $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
          }
          $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
          $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
          $('.dropdown-choices').find('li').click(function(e) {
            var the_choice = $(this).html();
            $('.dropdown-selector').parent().find('input').val(the_choice);
            $('.dropdown-selector').parent().find('label').html(the_choice);
            $('.dropdown-backdrop').click();
          });
        });
        $('.dropdown-backdrop').click(function() {
          $(this).fadeOut(300, 'easeOutCubic');
          $('.dropdown-choices').fadeOut(300, 'easeOutCubic');
        });

        // Initializes label placeholders
        $('#contact-us .form-part1 label').each(function() {
          var value = $(this).parent().find('input').val();
          if(value != '' && $(this).parent().hasClass('subject-field') != true) {
            $(this).animate({
              'left': '240px',
              'opacity': '0.3' }, 300, 'easeOutBack');
          }
        });
        $('#contact-us .form-part2 label').each(function() {
          var value = $(this).parent().find('textarea').val();
          if(value != '') {
            $(this).animate({
              'top': '-20px',
              'opacity': '0.3' }, 300, 'easeOutBack');
          }
        });
        // Hides label placeholder on focus
        $('#contact-us input').focus(function() {
          $(this).parent().find('label').animate({
            'left': '240px',
            'opacity': '0.3' }, 300, 'easeOutBack');
        });
        // Show label placeholder on blur
        $('#contact-us input').blur(function() {
          if($(this).val() == '') {
            $(this).parent().find('label').animate({
              'left': '5px',
              'opacity': '1' }, 300, 'easeInBack');
          }
        });
        // Hides label placeholder on focus
        $('#contact-us textarea').focus(function() {
          $(this).parent().find('label').animate({
            'top': '-20px',
            'opacity': '0.3' }, 300, 'easeOutBack');
        });
        // Show label placeholder on blur
        $('#contact-us textarea').blur(function() {
          if($(this).val() == '') {
            $(this).parent().find('label').animate({
              'top': '0',
              'opacity': '1' }, 300, 'easeInBack');
          }
        });
  <?php if(is_front_page()): ?>
        // Displays / Hides the dropdown choices
        $('.dropdown-selector, .subject-field label').click(function() {
          
          if($('.dropdown-choices').length == 0) {
            $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
          }
          var position = $(this).parent().offset();
          var item_height = $(this).parent().height() - 18;
          var item_width = $(this).parent().width();
          if($(this).is('.dropdown-selector') == true)
            var choices = $(this).attr('data-choices');
          else
            var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
          choices = choices.split(',');
        
          $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
          var choice;
          $('.dropdown-choices-list').html('');
          for (choice in choices) {
            $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
          }
          $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
          $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
          $('.dropdown-choices').find('li').click(function(e) {
            var the_choice = $(this).html();
            $('.dropdown-selector').parent().find('input').val(the_choice);
            $('.dropdown-selector').parent().find('label').html(the_choice);
            $('.dropdown-backdrop').click();
          });
        });
        $('.dropdown-backdrop').click(function() {
          $(this).fadeOut(300, 'easeOutCubic');
          $('.dropdown-choices').fadeOut(300, 'easeOutCubic');
        })
  <?php endif; ?>

      // Creates the user account on submit
      $('#contact-us').submit(function(e) {
        e.preventDefault();
        var error = 0;
        var error_code = {
          is_missing_field    : false,
          is_blacklisted      : false,
          is_invalid_email    : false
        }
        var user_name = $('#user_name').val();
        var user_email = $('#user_email').val();
        var message_subject = $('#message_subject').val();
        var message_content = $('#message_content').val();
        var user_lang = $('#user_lang').val();
        var user_ip = $('#user_ip').val();
        //var user_lang = $('#cc_user_lang').val();

        // Removes error messages (from previous attempts)
        $('.is-error-code').slideUp(200, 'easeOutCubic');

        $('.mandatory-field').each(function() {
          if($(this).val() == '') {
            $(this).parent().addClass('has_error');
            $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
            error++;
            error_code.is_missing_field = true;
          } else {
            if($(this).is('#user_email') && !isValidEmailAddress(user_email)) {
              $(this).parent().addClass('has_error');
              $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
              error++;
              error_code.is_invalid_email = true;
            } else {
              $(this).parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
              $(this).parent().removeClass('has_error');         
            }
          }
        }); 
      
        if(error > 0) {
          if(error_code.is_invalid_email) $('.is-invalid-email').slideDown(500, 'easeOutCubic');
          if(error_code.is_missing_field) $('.is-missing-field').slideDown(500, 'easeOutCubic');
          return false;
        } else {
          var data = {
            action: 'dt_contact_us',
            user_name: user_name,
            user_email: user_email,
            message_subject: message_subject,
            message_content: message_content,
            user_lang: user_lang,
            user_ip: user_ip
          };
          $('#submit_invite').hide('slide', {direction: 'down'}, 300);
          $('#ajaxsave').show('slide', {direction: 'up'}, 300);
          $.post("<?php echo admin_url('admin-ajax.php'); ?>", data,
          function(response){
            $('#ajaxsave').hide('slide', {direction: 'down'}, 300);
            $('#submit_invite').show('slide', {direction: 'up'}, 300);
            $('.mandatory-field').removeClass('has_error');
            if(response == 'is_failure') 
              $('.is-failure').slideDown(500, 'easeOutCubic');
            if(response == 'is_ok') {
              $('.mandatory-field').val('');
              $('.is-success').slideDown(500, 'easeOutCubic');
            }
            
          });   
          return false;
        }
      });

<?php endif; ?>



<?php if(is_page('our-partners')) : ?>

        // Initializes label placeholders
        $('#contact-us .form-part1 label').each(function() {
          var value = $(this).parent().find('input').val();
          if(value != '' && $(this).parent().hasClass('subject-field') != true) {
            $(this).animate({
              'left': '240px',
              'opacity': '0.3' }, 300, 'easeOutBack');
          }
        });
        $('#contact-us .form-part2 label').each(function() {
          var value = $(this).parent().find('input').val();
          if(value != '' && $(this).parent().hasClass('subject-field') != true) {
            $(this).animate({
              'left': '150px',
              'opacity': '0.3' }, 300, 'easeOutBack');
          }
        });
        // Hides label placeholder on focus
        $('#contact-us .form-part1 input').focus(function() {
          $(this).parent().find('label').animate({
            'left': '240px',
            'opacity': '0.3' }, 300, 'easeOutBack');
        });
        // Hides label placeholder on focus
        $('#contact-us .form-part2 input').focus(function() {
          $(this).parent().find('label').animate({
            'left': '150px',
            'opacity': '0.3' }, 300, 'easeOutBack');
        });
        // Show label placeholder on blur
        $('#contact-us input').blur(function() {
          if($(this).val() == '') {
            $(this).parent().find('label').animate({
              'left': '5px',
              'opacity': '1' }, 300, 'easeInBack');
          }
        });

      // Creates the user account on submit
      $('#contact-us').submit(function(e) {
        e.preventDefault();
        var error = 0;
        var error_code = {
          is_missing_field    : false,
          is_blacklisted      : false,
          is_invalid_email    : false
        }
        var user_name = $('#user_name').val();
        var user_email = $('#user_email').val();
        var user_restaurant = $('#user_restaurant').val();
        var user_phone = $('#user_phone').val();
        var user_lang = $('#user_lang').val();
        var user_ip = $('#user_ip').val();
        //var user_lang = $('#cc_user_lang').val();

        // Removes error messages (from previous attempts)
        $('.is-error-code').slideUp(200, 'easeOutCubic');

        $('.mandatory-field').each(function() {
          if($(this).val() == '') {
            $(this).parent().addClass('has_error');
            $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
            error++;
            error_code.is_missing_field = true;
            $(this).blur();
          } else {
            if($(this).is('#user_email') && !isValidEmailAddress(user_email)) {
              $(this).parent().addClass('has_error');
              $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
              error++;
              error_code.is_invalid_email = true;
              $(this).blur();
            } else {
              $(this).parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
              $(this).parent().removeClass('has_error');         
            }
          }
        }); 
      
        if(error > 0) {
          if(error_code.is_invalid_email) $('.is-invalid-email').slideDown(500, 'easeOutCubic');
          if(error_code.is_missing_field) $('.is-missing-field').slideDown(500, 'easeOutCubic');
          return false;
        } else {
          var data = {
            action: 'dt_our_partners',
            user_name: user_name,
            user_email: user_email,
            user_restaurant: user_restaurant,
            user_phone: user_phone,
            user_lang: user_lang,
            user_ip: user_ip
          };
          $('#submit_invite').hide('slide', {direction: 'down'}, 300);
          $('#ajaxsave').show('slide', {direction: 'up'}, 300);
          $.post("<?php echo admin_url('admin-ajax.php'); ?>", data,
          function(response){
            $('#ajaxsave').hide('slide', {direction: 'down'}, 300);
            $('#submit_invite').show('slide', {direction: 'up'}, 300);
            $('.mandatory-field').removeClass('has_error');
            if(response == 'is_failure') 
              $('.is-failure').slideDown(500, 'easeOutCubic');
            if(response == 'is_ok') {
              $('.mandatory-field').val('');
              $('.is-success').slideDown(500, 'easeOutCubic');
            }
            
          });   
          return false;
        }
      });

<?php endif; ?>



      $("#sign-in-text-div").mouseenter(function() {
        $("#sign-in-text-div").slideUp(500, 'easeOutCubic');
        $("#sign-in-form-div").slideDown(500, 'easeOutCubic');
      });
      $('#header').mouseleave(function() {
        if($("#sign-in-text-div").is(":hidden")) {
          $("#sign-in-form-div").slideUp(500, 'easeOutCubic');
          $("#sign-in-text-div").slideDown(500, 'easeOutCubic');
        }
      });
  
      // Hides label placeholder on focus
      $('#get-signin-form input').focus(function() {
        $(this).parent().find('label').animate({
          'left': '210px',
          'opacity': '0.0' }, 300, 'easeOutBack');
      });
      // Show label placeholder on blur
      $('#get-signin-form input').blur(function() {
        if($(this).val() == '') {
          $(this).parent().find('label').animate({
            'left': '5px',
            'opacity': '1' }, 300, 'easeInBack');
        }
      });
  
      // Creates the user account on submit
      $('#get-signin-form').submit(function(e) {
        e.preventDefault();
        var error = 0;
        var error_code = {
          is_missing_field    : false,
          is_blacklisted      : false,
          is_invalid_email    : false
        }
        var user_email = $('#user_login_email').val();
        var user_pwd = $('#user_login_password').val();

        $('.login-mandatory-field').each(function() {
          if($(this).val() == '') {
            $(this).parent().addClass('has_error');
            $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
            error++;
            error_code.is_missing_field = true;
          } else {
            if($(this).is('#user_email') && !isValidEmailAddress(user_email)) {
              $(this).parent().addClass('has_error');
              $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
              error++;
              error_code.is_invalid_email = true;
            } else {
              $(this).parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
              $(this).parent().removeClass('has_error');         
            }
          }
        }); 
    
        if(error > 0) {
          console.log('This should be displayed only once upon submit.');
          return false;
        } else {
          var data = {
            action: 'dt_login_submit',
            user_email: user_email,
            user_password: user_pwd,
            security: $('#security-login').val()
          };
      
          $.post("<?php echo admin_url('admin-ajax.php'); ?>", data,
          function(response){
            if(response == 'is_ok')
              location.reload();
            else
              alert(response);
          });   
      
          return false;
        }
      });
  
      // Creates the user account on submit
      $('#account-settings').submit(function(e) {
        //e.preventDefault();
        var error = 0;
        var error_code = {
          is_missing_field    : false,
          is_blacklisted      : false,
          is_invalid_email    : false
        }
        var user_email = $('#user_email').val();
        var user_password = $('#user_password').val();
        var user_password2 = $('#usewr_confirmpassword').val();
    
        $('.mandatory-field').each(function() {
          if($(this).val() == '') {
            $(this).parent().addClass('has_error');
            $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
            error++;
            error_code.is_missing_field = true;
          } else {
            if($(this).is('#user_email') && !isValidEmailAddress(user_email)) {
              $(this).parent().addClass('has_error');
              $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
              error++;
              error_code.is_invalid_email = true;
            } else {
              $(this).parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
              $(this).parent().removeClass('has_error');           
            }
          }
        }); 
    
        if(user_password2 != user_password) { 
          $("#user_password").parent().addClass('has_error');
          $("#user_password").parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
    
          $("#usewr_confirmpassword").parent().addClass('has_error');
          $("#usewr_confirmpassword").parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
          error++;
          error_code.is_missing_field = true;
        } else {
          $("#user_password").parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
          $("#user_password").parent().removeClass('has_error');     
          $("#usewr_confirmpassword").parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
          $("#usewr_confirmpassword").parent().removeClass('has_error');         
        }  
    
        if(error > 0) {
          console.log('This should be displayed only once upon submit.');
          return false;
        } else {
      
          return true;
        }
      });
  
  
  
      $('.image').load(function (e) {
        $(this).parent().fadeIn(800);
      });

    });
</script>
<?php
wp_footer();
?>
</body>
</html>
