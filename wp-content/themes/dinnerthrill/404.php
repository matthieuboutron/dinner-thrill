<?php
get_header(); ?>

<?php get_template_part('part-section-title'); ?>

<div class="inside-pane">
	<?php get_sidebar(); ?>

	<div id="about-section-content" class="content-pane">
			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Not Found', 'dinnerthrill' ); ?></h1>
				<div class="entry-content">
					<p><?php _e( 'Apologies, but the page you requested could not be found.', 'dinnerthrill' ); ?></p>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->
	</div>

</div>

<?php get_footer(); ?>
