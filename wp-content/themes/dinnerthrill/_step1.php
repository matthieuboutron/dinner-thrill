<form id="reserv-step1" action="" method="get">
  <input type="hidden" name="reserv_hash" value="<?php echo $reserv_hash;?>"/>
           <table width="235" border="0" cellspacing="0" cellpaing="0">
            <tr>
              <td width="235" height="80" colspan="2" class="valign-t"><h2>Make a<br>
                  <strong>reservation</strong></h2>
                <p><img src="<?php echo bloginfo("template_directory"); ?>/img/sep-reserv1.gif" width="235" height="15" alt=" "></p>
                <table width="235" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="perceL"><img src="<?php echo bloginfo("template_directory"); ?>/img/30percent.jpg" width="73" height="32"></td>
                    <td class="perceR">off all <br/>
                      food & drink</td>
                  </tr>
                </table>
                <p><img src="<?php echo bloginfo("template_directory"); ?>/img/sep-reserv2.png" width="235" height="15" alt=" "></p></td>
            </tr>
            </table>
      <div class="step1">     
        <table width="235" border="0" cellspacing="0" cellpadding="0">
           
           <tr id="errs" class="error" style="display:none;">
              <td width="65" class="valign-m"></td>
              <td width="170">Please correct fields in red</td>
            </tr>
           
           <tr>
              <td width="65" class="valign-m"><img src="<?php echo bloginfo("template_directory"); ?>/img/traduction/<?php echo ICL_LANGUAGE_CODE; ?>/for.png" width="46" height="45" alt="for"></td>
              <td width="170"><div class="field-wrap people-field" rel="people-field">
                  <div class="dropdown-selector"></div>
                  <label for="select_people"><?php echo $reserv?$reserv->personnes." People":"People"?></label>
                  <input id="select_people" type="hidden" value="<?php echo $reserv?$reserv->personnes:"null";?>" name="select_people">
                  <div class="error-sign"></div>
                </div></td>
            </tr>
            <tr class="collapsable" style="display:none;" rel="people-field">
              <td width="65">&nbsp;</td>
              <td width="170">
                <div class="slidable">
              <ul class="people">
                <li><a rel="1" href="#_"> 1 Person</a></li>
                <li><a rel="2" href="#_"> 2 People</a></li>
                <li><a rel="3" href="#_"> 3 People</a></li>
                <li><a rel="4" href="#_"> 4 People</a></li>
                <li><a rel="5" href="#_"> 5 People</a></li>
                <li><a rel="6" href="#_"> 6 People</a></li>
                <li><a rel="7" href="#_"> 7 People</a></li>
                <li><a rel="8" href="#_"> 8 People</a></li>
              </ul>
                </div>
              </td>
            </tr>

           <tr id="err-date" class="error" style="display:none;">
              <td width="65" class="valign-m"></td>
              <td width="170">Please correct the date</td>
            </tr>
           
<tr>
            <td width="65" class="valign-m"><img src="<?php echo bloginfo("template_directory"); ?>/img/traduction/<?php echo ICL_LANGUAGE_CODE; ?>/on.png" width="46" height="45" alt="on"></td>
            <td width="170">
            <div class="field-wrap date-field" rel="date-field">
                  <div id="dropdown-date" class="dropdown-selector" data-choices=""></div>
                  <input id="select_date" rel="date-field" readonly="readonly" autocomplete="off" type="text" value="<?php echo $reserv?$reserv->date:"Date";?>" name="select_date">
                  <div class="error-sign"></div>
               </div></td>
</tr>
<tr class="collapsable" style="display:none;"  rel="date-field"><td colspan=2><span id="date-target"/></td></tr>

           <tr id="err-time" class="error" style="display:none;">
              <td width="65" class="valign-m"></td>
              <td width="170">Please correct this value</td>
            </tr>

<tr>
              <td width="65" class="valign-m"><img src="<?php echo bloginfo("template_directory"); ?>/img/traduction/en/for.png" width="46" height="45" alt="for"></td>
              <td width="170"><div class="field-wrap meals-field" rel="meals-field">
                  <div class="dropdown-selector"></div>
                  <label for="select_meals"><?php echo $reserv?substr($reserv->time,0,-3):__('Time', 'dinnerthrill'); ?></label>
                  <input id="select_meals" type="hidden" value="<?php echo $reserv?str_replace(":","",substr($reserv->time,0,-3)):"null";?>" name="select_meals">
                  <div class="error-sign"></div>
                </div></td>
            </tr>
            <tr class="collapsable" style="display:none;" rel="meals-field">
              <td width="235" colspan="2" >
                <div class="slidable">
                  <ul class="time">
                <?php $times = get_the_terms($post->ID, 'booking_time'); 
                foreach($times as $t):?>
                <?php echo '<li><a rel="'.$t->slug.'" href="#_">'.$t->name.'</a></li>';?>
                <?php endforeach;?>
                </ul>
                <p class="time">Available  |  <em>Selected</em></p>
                </div>
                </td>
            </tr>
            <tr>
              <td width="235" class="valign-b" colspan="2" height="60"><a id="step1-submit" href="#_" class="next_step">Continue</a></td>
            </tr>

        </table>
        </div>
      </form>
