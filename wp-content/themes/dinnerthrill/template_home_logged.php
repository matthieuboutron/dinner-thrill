<div id="main-pane" class="template_homepage">
    <?php get_template_part('home', 'highlights'); ?>
    <div id="mod-reservation">
      <form id="reserv-step1" action="" method="get">
        <table width="920" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="210"><?php echo __("I'd like to make<br>a reservation:", 'dinnerthrill'); ?></td>
            <td width="55" class="valign-m"><img src="<?php echo bloginfo("template_directory"); ?>/img/traduction/<?php echo ICL_LANGUAGE_CODE; ?>/for.png" width="46" height="45" alt="for"></td>
            <td width="115"><div class="field-wrap people-field">
                <div class="dropdown-selector" id="dropdown-people" data-choices="<?php echo __('1 Person', 'dinnerthrill'); ?>, <?php echo __('2 People', 'dinnerthrill'); ?>, <?php echo __('3 People', 'dinnerthrill'); ?>, <?php echo __('4 People', 'dinnerthrill'); ?>, <?php echo __('5 People', 'dinnerthrill'); ?>, <?php echo __('6 People', 'dinnerthrill'); ?>, <?php echo __('7 People', 'dinnerthrill'); ?>, <?php echo __('8 People', 'dinnerthrill'); ?>"></div>
                <label for="select_people"><?php echo __('People', 'dinnerthrill'); ?></label>
                <input id="select_people" type="hidden" value="null" name="select_people">
                <div class="error-sign"></div>
              </div></td>
            <td width="35">&nbsp;</td>
            <td width="55" class="valign-m"><img src="<?php echo bloginfo("template_directory"); ?>/img/traduction/<?php echo ICL_LANGUAGE_CODE; ?>/on.png" width="46" height="45" alt="on"></td>
            <td width="115"><div class="field-wrap date-field">
                  <div id="dropdown-date" class="dropdown-selector" data-choices=""></div>
                  <input id="select_date" rel="date-field" readonly="readonly" autocomplete="off" type="text" value="Date" name="select_date">
                  <div class="error-sign"></div>

              </div></td>
            <td width="35">&nbsp;</td>
            <td width="55" class="valign-m"><img src="<?php echo bloginfo("template_directory"); ?>/img/traduction/<?php echo ICL_LANGUAGE_CODE; ?>/for.png" width="46" height="45" alt="for"></td>
            <td width="115"><div class="field-wrap meals-field">
             <?php $times = get_terms('booking_time', array('fields' => 'names', 'hide_empty' => 0)); ?>
                <div class="dropdown-selector" id="dropdown-time" data-choices="<?php echo implode(', ', $times); ?>"></div>
                <label for="select_meals"><?php echo __('Time', 'dinnerthrill'); ?></label>
                <input id="select_meals" type="hidden" value="null" name="select_meals">
                <div class="error-sign"></div>
              </div></td>
            <td width="35">&nbsp;</td>
            <td class="valign-m"><a id="home-step1-submit" href="#_"><img src="<?php echo bloginfo("template_directory"); ?>/img/traduction/<?php echo ICL_LANGUAGE_CODE; ?>/btn-go.png" alt="go"></a></td>
          </tr>
        </table>
      </form>
    </div>
    <div id="filters">
    
         <form action="" method="get">   <div class="field-wrap neighborhood-field">
             <?php $neighborhoods = get_terms('neighborhood', array('fields' => 'names', 'hide_empty' => 0)); ?>
                <div class="dropdown-selector" id="dropdown-neighborhood" data-choices="<?php echo implode(', ', $neighborhoods); ?>"></div>
                <label for="select_neighborhood"><?php echo __('Neighborhood', 'dinnerthrill'); ?></label>
                <input id="select_neighborhood" type="hidden" value="null" name="select_neighborhood">
              </div>
    
             <?php $cuisines = get_terms('cuisine', array('fields' => 'names', 'hide_empty' => 0)); ?>
            <div class="field-wrap cuisine-field">
                <div class="dropdown-selector" id="dropdown-cuisine" data-choices="<?php echo implode(', ', $cuisines); ?>"></div>
                <label for="select_cuisine"><?php echo __('Cuisine', 'dinnerthrill'); ?></label>
                <input id="select_cuisine" type="hidden" value="null" name="select_cuisine">
              </div>
    
             <?php $scenes = get_terms('scene', array('fields' => 'names', 'hide_empty' => 0)); ?>
            <div class="field-wrap scene-field">
                <div class="dropdown-selector" id="dropdown-scene" data-choices="<?php echo implode(', ', $scenes); ?>"></div>
                <label for="select_scene"><?php echo __('Scene', 'dinnerthrill'); ?></label>
                <input id="select_scene" type="hidden" value="null" name="select_scene">
              </div>
    
             <?php $costs = get_terms('cost', array('fields' => 'names', 'hide_empty' => 0)); ?>
            <div class="field-wrap cost-field">
                <div class="dropdown-selector" id="dropdown-cost" data-choices="<?php echo implode(', ', $costs); ?>"></div>
                <label for="select_cost"><?php echo __('Cost', 'dinnerthrill'); ?></label>
                <input id="select_cost" type="hidden" value="null" name="select_cost">
              </div>
    
            <div class="btn-wrap">
                <input name="jqFilter" id="jqFilter" type="image" src="<?php echo bloginfo("template_directory"); ?>/img/traduction/<?php echo ICL_LANGUAGE_CODE; ?>/btn-filters-find.gif" alt="find">
            </div>
    
    </form>
    </div>
    <div id="results">
      <div class="title">
        <div class="left">
          <h4><?php echo __('Available Tonight', 'dinnerthrill'); ?></h4>
        </div>
        <div class="right"><a href="#_" id="jqSeeAll" class="yellowarrow"><?php echo __('Browse All Restaurants', 'dinnerthrill'); ?></a></div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <div id="jqResults">
        <?php $restaurants = get_posts('post_type=restaurant&numberposts=6&orderby=rand'); ?>
        <?php foreach($restaurants as $post): setup_postdata($post); ?>
          <?php get_template_part('home', 'restaurant'); ?>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="clear-it"></div>
