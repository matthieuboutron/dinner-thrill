<?php get_header(); ?>
 
<div id="main-pane" class="template_results">
    <div id="highlights" style="background:url(../img/temp-highlights-results-photo.jpg) no-repeat">
      <div class="bandeau">
        <h3>FOUR-COURSE MEAL AT MICHELIN-STARRED LAUT</h3>
        <ul>
          <li>12/18/11</li>
          <li><a href="#_" class="yellowarrow">book today</a> </li>
        </ul>
      </div>
      <div id="mod-reservation">
      <form action="" method="get">
     
        <table width="235" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="235" height="80" colspan="2" class="valign-t">I'd like to make <br>
              a reservation:</td>
           </tr>
           <tr>
            <td width="65" class="valign-m"><img src="../img/traduction/en/for.png" width="46" height="45" alt="for"></td>
            <td width="170"><div class="field-wrap people-field">
                <div class="dropdown-selector" data-choices="1 Person, 2 People, 3 People, 4 People, 5 People, 6 People, 7 People, 8 People"></div>
                <label for="message_subject">People</label>
                <input id="message_subject" type="hidden" value="null" name="message_subject">
                <div class="error-sign"></div>
              </div></td>
</tr>
<tr>
            <td width="65" class="valign-m"><img src="../img/traduction/en/on.png" width="46" height="45" alt="on"></td>
            <td width="170"><div class="field-wrap date-field">
                <div class="dropdown-selectorsss" data-choices=""></div>
                <label for="select_date">DD/MM/YY</label>
                <input id="select_date" type="hidden" value="null" name="select_date">
                <div class="error-sign"></div>
              </div></td>
</tr>
<tr>
            <td width="65" class="valign-m"><img src="../img/traduction/en/for.png" width="46" height="45" alt="for"></td>
            <td width="170"><div class="field-wrap meals-field">
                <div class="dropdown-selector" data-choices="Lunch, Dinner"></div>
                <label for="select_meals">Lunch</label>
                <input id="select_meals" type="hidden" value="null" name="select_meals">
                <div class="error-sign"></div>
              </div></td>
</tr>
<tr>
            <td width="235" class="valign-b" colspan="2" height="60"><input name="" type="image" src="../img/traduction/en/btn-go.png" alt="go"></td>
          </tr>
        </table>
      </form>
    </div>
      
      
    </div>
    
    <div id="filters">
        <?php
            $neighborhood_terms = get_terms("neighborhood", 'hide_empty=1');
            $cost_terms = get_terms("cost", 'hide_empty=1');
            $cuisine_terms = get_terms("cuisine", 'hide_empty=1');
            $scene_terms = get_terms("scene", 'hide_empty=1');
            
            $filter = false;
            
            if($_GET["neighborhood"] != "") {
                $neighb = esc_attr( $_GET["neighborhood"] );
                $arr = array();
                foreach ($neighborhood_terms as $term) {
                    $arr[] = $term->slug;
                }
                $selectedN = "";
                if(in_array($neighb, $arr)) {
                    $filter = true;
                    $selectedN = $neighb;
                }
            }
            
            if($_GET["cost"] != "") {
                $cost = esc_attr( $_GET["cost"] );
                $arr = array();
                foreach ($cost_terms as $term) {
                    $arr[] = $term->slug;
                }
                $selectedC = "";
                if(in_array($cost, $arr)) {
                    $filter = true;
                    $selectedC = $cost;
                }
            }
            
            if($_GET["scene"] != "") {
                $scene = esc_attr( $_GET["scene"] );
                $arr = array();
                foreach ($scene_terms as $term) {
                    $arr[] = $term->slug;
                }
                $selectedS = "";
                if(in_array($scene, $arr)) {
                    $filter = true;
                    $selectedS = $scene;
                }
            }
            
            if($_GET["cuisine"] != "") {
                $cuisine = esc_attr( $_GET["cuisine"] );
                $arr = array();
                foreach ($cuisine_termsi as $term) {
                    $arr[] = $term->slug;
                }
                $selectedCuisine = "";
                if(in_array($cuisine, $arr)) {
                    $filter = true;
                    $selectedCuisine = $cuisine;
                }
            }

        
        ?>
         <form action="" method="get">   
              <div class="field-wrap neighborhood-field">
                <div class="dropdown-selector" data-choices="Plateau, Downtown"></div>
                <label for="select_neighborhood">Neighborhood</label>
                <input id="select_neighborhood" type="hidden" value="null" name="select_neighborhood">
              </div>
    
            <div class="field-wrap cuisine-field">
                <div class="dropdown-selector" data-choices="Chinese, French"></div>
                <label for="select_cuisine">Cuisine</label>
                <input id="select_cuisine" type="hidden" value="null" name="select_cuisine">
              </div>
    
            <div class="field-wrap scene-field">
                <div class="dropdown-selector" data-choices="Scene 1, Scene 2"></div>
                <label for="select_scene">Scene</label>
                <input id="select_scene" type="hidden" value="null" name="select_scene">
              </div>
    
            <div class="field-wrap cost-field">
                <div class="dropdown-selector" data-choices="$, $$, $$$, $$$$"></div>
                <label for="select_cost">Cost</label>
                <input id="select_cost" type="hidden" value="null" name="select_cost">
              </div>
    
            <div class="btn-wrap">
                <input name="" type="image" src="../img/traduction/en/btn-filters-find.gif" alt="find">
                
              </div>
        </form>
    </div>
    
    <?php 
 
    $args = array( 'post_type' => "restaurant", 
                    "order" => "desc",
                    'suppress_filters' => false,
                    'post_status'     => 'publish');
    
    if($filter) {
        $args["tax_query"] = array();
        
        if($selectedN != "") {
            $args["tax_query"][] = array(
                                    'taxonomy' => 'neighborhood',
                                    'terms' => array($selectedN),
                                    'field' => 'slug',
                            );
        }
        
        if($selectedC != "") {
            $args["tax_query"][] = array(
                                    'taxonomy' => 'cost',
                                    'terms' => array($selectedC),
                                    'field' => 'slug',
                            );
        }
        
        if($selectedCuisine != "") {
            $args["tax_query"][] = array(
                                    'taxonomy' => 'cuisine',
                                    'terms' => array($selectedCuisine),
                                    'field' => 'slug',
                            );
        }
        
        if($selectedS != "") {
            $args["tax_query"][] = array(
                                    'taxonomy' => 'scene',
                                    'terms' => array($selectedS),
                                    'field' => 'slug',
                            );
        }
        
    }
    
    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ) :  ?>

    
    <div id="results">
      <div class="title">
        <div class="left">
          <h4>Your Results</h4>
        </div>
          <div class="right"><strong>30% OFF</strong> <?php echo sprintf(__("at %d matching restaurants"), $loop->post_count); ?> </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
      
      <?php
      $k = 0;
      while ( $loop->have_posts() ) : $loop->the_post();
        $k++;
        $itemClass = "";
        if($k == $loop->post_count)
            $itemClass = " last";
      
      ?>
      
      <div class="item<?php echo $itemClass;?>">
        <div class="photo">
            <a href="<?php echo get_post_permalink(get_the_ID()); ?>"><?php echo get_the_post_thumbnail(get_the_ID(), "restaurant-thumbnail-image"); ?></a>
        </div>
        <div class="infos">
          <div class="left">
            <ul>
              <li><a href="<?php echo get_post_permalink(get_the_ID()); ?>"><strong><?php the_title(); ?></strong></a></li>
              <li><?php echo getNeighborhoodForRestaurant(get_the_ID()); ?></li>
              <li><?php echo getCuisineForRestaurant(get_the_ID())." (".  getCostForRestaurant(get_the_ID()).")"; ?></li>
            </ul>
          </div>
          <div class="right">
              
        <?php
        $address = getCustomValue(get_the_ID(), "address");
        if($address != ""): ?>
            <?php echo $address; ?></br>
        <?php endif; ?>   
        <?php 
        $city = getCustomValue(get_the_ID(), "city");
        if($city != ""): ?>
            <?php echo $city; ?>
        <?php endif; ?>
              
          </div>
          <div class="clear"></div>
          <p>
              <?php   the_content(); ?>
          </p>
        </div>
        <a href="<?php echo get_post_permalink(get_the_ID()); ?>" class="yellowarrow">
            <?php _e("See Details"); ?></a>
      </div>
      
    
      
    <?php endwhile;?>
 <?php endif; ?>
   
      
      
    </div>
    <div class="clear-it"></div>
  </div>

<?php get_footer(); ?>