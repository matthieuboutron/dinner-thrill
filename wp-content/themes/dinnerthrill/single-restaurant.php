<?php
/**
 * The Template for displaying all single restaurant.
 *
 */
get_header();
?>
<div id="main-pane" class="template_detail">

  <?php
  $thumbnail = '';
  $post_image_id = get_post_thumbnail_id(get_the_ID());
  if($post_image_id)
  {
    $thumbnail = wp_get_attachment_image_src($post_image_id,
            "large-restaurant-image");
    if($thumbnail)
      (string) $thumbnail = $thumbnail[0];
  }
  ?>
  <div id="highlights" style="background:url(<?php echo $thumbnail; ?>) no-repeat">
    <div class="bandeau">
      <h1><?php the_title(); ?></h1>
    </div>
    <div id="mod-reservation">
<?php if( $_GET["rhash"] )
  echo reload_reservation($_GET['rhash']);
 else
   require_once(get_theme_root()."/".get_current_theme()."/_step1.php");
?>
    </div>


  </div>

  <div id="sidebar">
    <div class="fiche">
      <div class="neigh">
        <div>
          <h4><?php _e("Neighborhood"); ?></h4>
          <p><?php echo getNeighborhoodForRestaurant(get_the_ID(),
          "</br>"); ?></p>
        </div>
      </div>
      <div class="cuisine"><div>
          <h4><?php _e("Cuisine"); ?></h4>
          <p><?php echo getCuisineForRestaurant(get_the_ID(),
          "</br>"); ?></p>
        </div></div>
      <div class="scene"><div>
          <h4><?php _e("Scene"); ?></h4>
          <p><?php echo getSceneForRestaurant(get_the_ID(),
          "</br>"); ?></p>
        </div></div>
      <div class="cost"><div>
          <h4><?php _e("Cost"); ?></h4>
          <p><?php echo getCostForRestaurant(get_the_ID(),
          "</br>"); ?></p>
        </div></div>

    </div>
    <h3>Details</h3>
    <p>
      <?php
      $completeAddress = "";
      $address = getCustomValue(get_the_ID(),
              "address");
      if($address != ""):
        ?>
        <?php echo $address;
        $completeAddress.= $address;
        ?></br>
      <?php endif; ?>   
      <?php
      $city = getCustomValue(get_the_ID(),
              "city");
      if($city != ""):
        ?>
        <?php echo $city;
        $completeAddress.= " " . $city;
        ?>,&nbsp;
<?php endif; ?>
    <?php
    $province = getCustomValue(get_the_ID(),
            "province");
    if($province != ""):
      ?>
      <?php echo $province . " ";
      $completeAddress.= " " . $province;
      ?>
    <?php endif; ?>
    <?php
    $pc = getCustomValue(get_the_ID(),
            "postal-code");
    if($pc != ""):
      ?>
      <?php echo $pc;
      $completeAddress.= " " . $pc;
      ?>
      <?php endif; ?>

    </p> 
      <?php
      $phone = getCustomValue(get_the_ID(),
              "phone");
      if($phone != ""):
        ?>
      <p><?php echo __("Phone") . ": " . $phone; ?></p>
      <?php endif; ?>
      <?php
      $cell = getCustomValue(get_the_ID(),
              "cell-phone");
      if($cell != ""):
        ?>
      <p><?php echo __("Cell") . ": " . $cell; ?></p>
<?php endif; ?>
<?php
$email = getCustomValue(get_the_ID(),
        "email");
if($email != ""):
  ?>
      <p><a href="mailto:<?php echo $email; ?>"><?php echo __("Email") . ": " . $email; ?></a></p>
    <?php endif; ?>
    <ul class="details">

    <?php $completeAddress = str_replace(" ",
            "+",
            $completeAddress); ?>
      <li><a target="_blank" href="http://maps.google.com/maps?f=d&geocode=&daddr=<?php echo $completeAddress ?>&ie=UTF8">Directions</a></li>
    <?php
    $menu_link = getCustomValue(get_the_ID(),
            "menu_link");
    if($menu_link != ""):
      ?>
        <li><a target="_blank" href="<?php echo $menu_link ?>"><?php _e("Menu") ?></a></li>
<?php endif; ?>
<?php
$website = getCustomValue(get_the_ID(),
        "website");
if($website != ""):
  ?>
        <li><a  target="_blank" href="<?php echo $website ?>"><?php _e("Website") ?></a></li>
<?php endif; ?>
    </ul>


    <div class="map">
      <iframe width="275" height="195" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;geocode=&amp;q=<?php echo $completeAddress; ?>&amp;aq=&amp;&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $completeAddress; ?>&amp;spn=0.011727,0.023603&amp;z=15&amp;iwloc=near&amp;output=embed"></iframe>
    </div>
  </div>
  <div id="detail-content">
    <h2><?php _e("Why We Love It"); ?></h2>
<?php
//mystérieusement ici the_content ne fonctionne pas...
echo apply_filters('the_content',
        $post->post_content);
?>

<?php
$press = getCustomValue(get_the_ID(),
        "press-reviews");
if($press != ""):
  ?>
      <h2><?php _e("Press &amp; Reviews"); ?></h2>
  <?php echo $press ?>
<?php endif; ?>


  </div>

  <div class="clear-it"></div>
  <!-- #main-pane -->

<?php get_footer(); ?>