<?php

// Configuration files
include_once('conf/backend.php');
include_once('conf/housekeeping.php');

//Mark addition
include_once('includes/profil.inc.php');
include_once('includes/restaurant.inc.php');

// Matthieu
include_once('includes/homepage_slider.inc.php');

// Julien
include_once(WP_PLUGIN_DIR.'/reserv/includes/functions.inc.php');


// Function modules
include_once('func-contact.php');

// Scripts
function dt_js() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
    wp_enqueue_script( 'jquery' );
    wp_register_script( 'jqueryui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js');
    wp_enqueue_script( 'jqueryui' );
    wp_register_script( 'slidesjs', get_bloginfo('template_directory') . '/js/slides.min.jquery.js' );
    wp_enqueue_script( 'slidesjs' );
    wp_register_script( 'glCalendar', get_bloginfo('template_directory') . '/js/glDatePicker.js' );
    wp_enqueue_script( 'glCalendar' );
}
add_action('wp_enqueue_scripts', 'dt_js');

// Sidebar Menu
register_nav_menu('sidebar', 'menu in sidebar');

// Create YouTube Embed code
function youtube($video_url, $width = 640, $height = 480, $class = NULL){
  $sanitize_url = str_replace ('watch?v=', 'embed/', $video_url);
  return '<iframe width="'.$width.'" height="'.$height.'" src="'.$sanitize_url.'?wmode=Opaque&rel=0" frameborder="0" allowfullscreen></iframe>';
}

// Shortens text to number of sentences.
function limit_sentences($text, $number_of_sentences) {
  preg_match('/^([^.!?]*[\.!?]+){0,'.$number_of_sentences.'}/', strip_tags($text), $abstract);
  echo $abstract[0];
}

// Shortens text to number of words.
function limit_words($text, $number_of_words) {
  preg_match('/^([^.!?\s]*[\.!?\s]+){0,'.$number_of_words.'}/', strip_tags($text), $abstract);
  echo $abstract[0];
}

// Creates a button with rounded corners
function btn($label, $url = '#', $class = NULL, $attr = NULL, $colorscheme = NULL) {
  return '
    <a class="button '.$colorscheme.' '.$class.'" href="'.$url.'" '.$attr.'>
      <div class="round-left"></div>
      <div class="button-center">
        <div class="button-content">
          '.$label.'
        </div>
      </div>
      <div class="round-right"></div>
    </a>
    ';
}

// Returns the directory path to the uploads directory
function get_upload_dir() {
	$updir = wp_upload_dir();
	return $updir['basedir'];
}
// Returns the public URL to the uploads directory
function get_upload_url() {
	$updir = wp_upload_dir();
	return $updir['baseurl'];
}

if ( ! function_exists( 'theme_comment' ) ) :
function theme_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">says:</span>', 'cookina' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'cookina' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'cookina' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'cookina' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'cookina' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'cookina' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

function get_page_id_by_slug( $page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

function string_contains( $haystack, $needle, $case_sensitive = false ) {
	if ( $case_sensitive )
		return ( strstr( $haystack, $needle ) != false);
	else
		return ( strstr( strtolower($haystack), strtolower($needle) ) != false );
}

function get_current_url() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {
		$pageURL .= "s";
	}
		
		$pageURL .= "://";

		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
}

                
function getTodayUntilNextWeek() {
    $today = date("Y-m-d");
    $results[] = date("d/m/y");
    for ($i=1;$i<=7;$i++)
        $results[] = date("d/m/y", strtotime(date("Y-m-d", strtotime($today)) . " +".$i." day"));
    return $results;
}


/* ajax for the restaurants on the homepage */
add_action('wp_ajax_load_restaurants', 'filter_restaurants');
add_action('wp_ajax_nopriv_load_restaurants', 'filter_restaurants');
function filter_restaurants() {
    $args = array( 'post_type' => 'restaurant', 
                   'numberposts' => 6
    );
    if (isset($_POST["neighborhood"]) && $_POST["neighborhood"] != 'null')
        $args['tax_query'][] = array( 'taxonomy'            => 'neighborhood',
                                      'field'               => 'name',
                                      'terms'               => $_POST["neighborhood"],
                                      'include_children'    => false);
    if (isset($_POST["cuisine"]) && $_POST["cuisine"] != 'null')
        $args['tax_query'][] = array( 'taxonomy'            => 'cuisine',
                                      'field'               => 'name',
                                      'terms'               => $_POST["cuisine"],
                                      'include_children'    => false);
    if (isset($_POST["scene"]) && $_POST["scene"] != 'null')
        $args['tax_query'][] = array( 'taxonomy'            => 'scene',
                                      'field'               => 'name',
                                      'terms'               => $_POST["scene"],
                                      'include_children'    => false);
    if (isset($_POST["cost"]) && $_POST["cost"] != 'null')
        $args['tax_query'][] = array( 'taxonomy'            => 'cost',
                                      'field'               => 'name',
                                      'terms'               => $_POST["cost"],
                                      'include_children'    => false);

    $restaurants = get_posts( $args );
    if ($restaurants)
    {
        foreach ($restaurants as $post){
            setup_postdata($post);
            $cuisineDetails = ''; $neighborhoodDetails = ''; $timesToBook = '';
            $neighborhood = get_the_terms($post->ID, 'neighborhood');
            if (is_array($neighborhood))
                foreach($neighborhood as $n)
                    $neighborhoodDetails .= $n->name;
            $cuisine = get_the_terms($post->ID, 'cuisine');
            if (is_array($cuisine))
                foreach($cuisine as $c)
                    $cuisineDetails .= $c->name;
            $times = getTimesIn2Hours();
            foreach($times as $t)
                $timesToBook .= '<li><a href="'.get_permalink($post->ID).'">'.$t.'</a></li>';
            echo '
          <div class="item">
              <div class="photo"><a href="'.get_permalink($post->ID).'">'.get_the_post_thumbnail($post->ID, 'restaurant-thumbnail-image').'</a></div>
            <div class="infos">
              <div class="left">
                <ul>
                  <li><a href="'.get_permalink($post->ID).'"><strong>'.$post->post_title.'</strong></a></li>
                  <li>'.$neighborhoodDetails.'</li>
                  <li>'.$cuisineDetails.'</li>
                </ul>
              </div>
              <div class="right">
                <ul>'.$timesToBook.'</ul>
              </div>
              <div class="clear"></div>
            </div>
            <a href="'.get_permalink($post->ID).'" class="yellowarrow">'.__('See All Times', 'dinnerthrill').'</a>
          </div>';
        }
    }
    else
        echo __('There is no restaurant matching your search.', 'dinnerthrill');
    die();
}