<form id="reserv-step2" action="" method="get">
<input type="hidden" name="reserv_hash" value="<?php echo $reserv_hash;?>"/>
  <table width="235" border="0" cellspacing="0" cellpaing="0">
            <tr>
              <td width="235" height="80" colspan="2" class="valign-t"><h2>Make a<br>
                  <strong>reservation</strong></h2>
                <p><img src="<?php echo bloginfo("template_directory"); ?>/img/sep-reserv1.gif" width="235" height="15" alt=" "></p>
                <table width="235" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="perceL"><img src="<?php echo bloginfo("template_directory"); ?>/img/30percent.jpg" width="73" height="32"></td>
                    <td class="perceR">off all <br/>
                      food & drink</td>
                  </tr>
                </table>
                <p><img src="<?php echo bloginfo("template_directory"); ?>/img/sep-reserv2.png" width="235" height="15" alt=" "></p></td>
            </tr>
            </table>
  <div class="step2">
    <table width="235" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="border_white" colspan="2">
        <div class="left">
          <ul>
            <li><?php echo $reserv->personnes;?> Ppl. </li>
            <li><?php echo substr($reserv->time,0,-3);?></li>
            <li><?php echo date('F j',strtotime($reserv->date));?></li>
          </ul>      
        </div>
        <div class="right">
        
        <a id="step2-submit-back" href="#_" class="yellowarrow">Edit</a>
        </div>
        <div class="clear"></div>
        
        </td>
      </tr>
      <tr>
        <td class="valign-m"><p class="big padT25">Amount Due </p></td>
        <td class="txt-r"><p class="big yellow padT25">$10</p></td>
      </tr>
      <tr>
        <td colspan="2">
          <img src="<?php echo bloginfo("template_directory"); ?>/img/sep-reserv1.gif" width="235" height="15" alt=" " class="valign-t"> <br>	
          <p class="big">reservation info</p>



        </td>
      </tr>
    <tr id="errs" class="error" style="display:none;">
              <td width="235" colspan="2">Please correct fields in red</td>
            </tr>
             <tr>
        <td colspan="2">
          <div class="field-wrap user-name-field">
            <input type="text" id="first_name" name="first_name" maxlength="200" class="mandatory-field" value="<?php echo $reserv->firstname?>" />
            <label for="first_name">First name</label>
            <div class="error-settings"></div></div></td>
      </tr>
      <tr>
        <td colspan="2">
          <div class="field-wrap user-name-field">
            <input type="text" id="last_name" name="last_name" maxlength="200" class="mandatory-field" value="<?php echo $reserv->lastname?>" />
            <label for="last_name">Last name</label>
            <div class="error-settings"></div></div></td>
      </tr>
      <tr>
        <td colspan="2">
          <div class="field-wrap user-name-field">
            <input type="text" id="phone_number" name="phone_number" maxlength="200" class="mandatory-field" value="<?php echo $reserv->phone?>" />
            <label for="phone_number">Phone number</label>
            <div class="error-settings"></div></div></td>
      </tr>
      <tr>
        <td colspan="2">
          <div class="field-wrap user-name-field">
            <input type="text" id="creditcard_number" name="creditcard_number" maxlength="16" class="mandatory-field" value="<?php echo $reserv->ccnum?>" />
            <label for="creditcard_number">VISA number</label>
            <div class="error-settings"></div></div></td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="235" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="60"><div class="field-wrap month-field" style="width:50px;">
                  <input style="width:50px;" type="text" id="month_card" maxlength="2" value="<?php echo $reserv->MM?>" name="month_card" class="mandatory-field" />
                  <label for="month_card">MM</label>
                  <div class="dropdown-selector" data-choices="01,02,03,04,05,06,07,08,09,10,11,12"></div>
                  <div class="error-sign"></div>
                </div></td>
              <td width="22">&nbsp;</td>
              <td width="60"><div class="field-wrap year-field" style="width:50px;">
                  <input style="width:50px;" type="text" id="year_card" maxlength="2" value="<?php echo $reserv->YY?>" name="year_card" class="mandatory-field" />
                  <label for="year_card">YY</label>
                  <div class="dropdown-selector" data-choices="12,13,14,15,16,17,18"></div>
                  <div class="error-sign"></div>
                </div></td>
              <td width="22">&nbsp;</td>
              <td width="71"><div class="field-wrap ccv-field" style="width:61px;">
                  <input style="width:50px;" type="text" id="ccv" name="ccv" maxlength="4" class="mandatory-field" value="" />
                  <label for="ccv">CCV</label>
                  <div class="error-settings"></div>
                </div></td>
            </tr>
          </table>


        </td>
      </tr> 
      <tr>
        <td colspan="2">
          <div class="field-wrap user-name-field">
            <input type="text" id="address" name="address" maxlength="100" class="mandatory-field" value="<?php echo $reserv->address?>" />
            <label for="address">Billing address</label>
            <div class="error-settings"></div></div></td>
      </tr>
      <tr>
        <td>
          <div class="field-wrap city-field">
            <input style="width:100px" type="text" id="city" name="city" maxlength="100" class="mandatory-field" value="<?php echo $reserv->city?>" />
            <label for="city">City</label>
            <div class="error-settings"></div></div></td>
        <td>
          <div class="field-wrap prov-field">
            <input style="width:100px" type="text" id="prov" name="prov" maxlength="2" class="mandatory-field" value="<?php echo $reserv->prov?>" />
            <label for="prov">Province</label>
            <div class="error-settings"></div></div></td>
      </tr>
      <tr>
        <td colspan="2">
          <div class="field-wrap user-name-field">
            <input  style="width:100px" type="text" id="postal_code" name="postal_code" maxlength="7" class="mandatory-field" value="<?php echo $reserv->billing_pc?>" />
            <label for="postal_code">Billing postal code</label>
            <div class="error-settings"></div></div></td>
      </tr>
      <tr>
        <td colspan="2">  <p><img src="<?php echo bloginfo("template_directory"); ?>/img/sep-reserv1.gif" width="235" height="15" alt=" "></p></td>
      </tr>
      <tr>
        <td><p class="big no_marg">Special request?</p></td>
        <td class="txt-r">
          <p class="no_marg"><a   id="show_special" href="#_" class="yellowarrow">Add</a></p>
        </td>
      </tr>
      <tr id="special_request_tr" style="display:none;">
        <td colspan="2"><textarea  name="special_request" id="special_request"><?php echo $reserv->special?></textarea></td>
      </tr>
      <tr>
        <td colspan="2">  <p><img src="<?php echo bloginfo("template_directory"); ?>/img/sep-reserv1.png" width="235" height="15" alt=" "></p></td>
      </tr>
      <td width="235" class="valign-b" colspan="2" height="60"><p class="padT25"><a id="step2-submit" href="#_" class="next_step">Reserve</a></p>
        <p class="big padT15">Read the terms</p>
        <ul class="readterms">
          <li>Cannot be combined with any other deal 
            or promotion.</li>
          <li> An 18% gratuity will be added to the 
            pre-discounted bill.</li>
        </ul>         </td>
      </tr>
    </table>

  </div>      
</form>
