<?php
/*
  Plugin Name: Reserv
 */


add_action('init',
        'reserv');
add_action('admin_menu',
        'reserv_menu');
add_action('wp_ajax_ajax-submit-step1',
        'ajax_submit_step1');
add_action('wp_ajax_ajax-submit-step2-back',
        'ajax_submit_step2_back');
add_action('wp_ajax_ajax-submit-step3-back',
        'ajax_submit_step3_back');
add_action('wp_ajax_ajax-submit-step2',
        'ajax_submit_step2');


add_action('wp_ajax_nopriv_ajax-submit-step1',
        'ajax_notlogged');


require_once(DIRNAME(__FILE__) . "/paypal.php");


// retreive an existing reserv from a hash;
function get_reserv($hash)
{
  global $wpdb;
  $reserv = $wpdb->get_results(
          $wpdb->prepare("
	SELECT * 
	FROM wp_reserv
	WHERE md5(CONCAT(id,'__dtdtdt'))=%s",
                  $hash));
  return $reserv[0];
}

// Code that executes when you submit step1 (time, date, place, how many people)
function ajax_submit_step1()
{
  global $current_user, $wpdb;

  $current_user = wp_get_current_user();

  $postID_val = $_POST['post_ID'];
  $post = get_post($_POST['post_ID']);

  $people_val = substr(trim($_POST['select_people']),
          0,
          1);
  $date_val = $_POST['select_date'];
  $time_val = str_replace(':',
          '',
          $_POST['select_meals']);
  $reserv_hash = $_POST['reserv_hash'];

  $time_ok = false;
  $date_ok = false;
  $people_ok = false;

  $times = get_the_terms(9,
          'booking_time');
  foreach($times as $t)
    if(strval($time_val) == $t->slug)
      $time_ok = true;


  // double validate on server-side
  $people_ok = $people_val >= 1 && $people_val <= 8;
  $date_ok = strlen($date_val) > 4; // temporaire...
  $form_ok = ($date_ok && $people_ok && $time_ok);

  if($form_ok)
  {
    if($reserv_hash)
    {
      $sql = $wpdb->prepare("UPDATE  `wp_reserv` (
									SET
                  `state`='Step1 ,
									`resto_id`=%d ,
									`user_id`=%d ,
									`personnes`=%d ,
									`date`=%s,
									`time`=%s WHERE md5(CONCAT(id,'__dtdtdt'))=%s LIMIT 1)",
              $postID_val,
              $current_user->ID,
              $people_val,
              $date_val,
              $time_val . "00",
              $reserv_hash);
      $ret = $wpdb->query($sql);
    }
    else
    {
      $sql = $wpdb->prepare("INSERT INTO  `wp_reserv` (
									`state` ,
									`resto_id` ,
									`user_id` ,
									`personnes` ,
									`date` ,
									`time` 
									) VALUES (
							'Step1',  '%d',  '%d',  '%d',  '%s',  '%s')",
              $postID_val,
              $current_user->ID,
              $people_val,
              $date_val,
              $time_val . "00");
      $ret = $wpdb->query($sql);
      $reserv_id = $wpdb->insert_id;
      $reserv_hash = md5($wpdb->insert_id . "__dtdtdt");
    }

    $reserv = get_reserv($reserv_hash);
    if($_POST['redirect'])
    {
      $response = get_bloginfo('wpurl') . "/restaurant/" . $post->post_name . "?rhash=" . $reserv_hash;
    }
    else
    {
      ob_start();
      require_once(get_theme_root() . "/" . get_current_theme() . "/_step2.php");
      $response = ob_get_clean();
    }
  }
  else
    $response = "Invalid post data:" . $people_val . "," . $time_val . "," . $date_val;

  header("Content-Type: text/html");
  echo $response;
  exit;
}

function ajax_submit_step2_back()
{
  global $current_user, $wpdb;

  $reserv_hash = $_POST['reserv_hash'];
  $post = get_post($_POST['post_ID']);

  $reserv = get_reserv($reserv_hash);
  
  ob_start();
  require_once(get_theme_root() . "/" . get_current_theme() . "/_step1.php");
  $response = ob_get_clean();
  header("Content-Type: text/html");
  echo $response;
  exit;
}

function ajax_submit_step3_back()
{
  global $current_user, $wpdb;

  $reserv_hash = $_POST['reserv_hash'];
  $post = get_post($_POST['post_ID']);

  $reserv = get_reserv($reserv_hash);
  
  ob_start();
  require_once(get_theme_root() . "/" . get_current_theme() . "/_step2.php");
  $response = ob_get_clean();
  header("Content-Type: text/html");
  echo $response;
  exit;
}

function ajax_submit_step2()
{
  global $current_user, $wpdb;

  $current_user = wp_get_current_user();

  $reserv_hash = $_POST['reserv_hash'];
  $reserv = get_reserv($reserv_hash);
  
  $post = get_post($reserv->resto_id);

  $lastname_val = $_POST['last_name'];
  $firstname_val = $_POST['first_name'];
  $phonenumber_val = $_POST['phone_number'];
  $creditcardnumber_val = $_POST['creditcard_number'];
  $monthcard_val = $_POST['month_card'];
  $yearcard_val = $_POST['year_card'];
  $ccv_val = $_POST['ccv'];
  $address_val = $_POST['address'];
  $city_val = $_POST['city'];
  $prov_val = $_POST['prov'];
  $postalcode_val = $_POST['postal_code'];
  $specialrequest_val = $_POST['special_request'];

  $form_ok = true;

  if($form_ok)
  {
    $sql = $wpdb->prepare("UPDATE `wp_reserv` SET

									`state` ='Step2',
                  firstname = '%s',
                  lastname = '%s',
                  phone = '%s',
                  ccnum = '%s',
                  MM = '%s',
                  YY = '%s',
                  billing_pc = '%s',
                  address = '%s',
                  city = '%s',
                  prov = '%s',
                  special = '%s' WHERE md5(CONCAT(id,'__dtdtdt'))=%s LIMIT 1",
            $firstname_val,
            $lastname_val,
            $phonenumber_val,
            $creditcardnumber_val,
            $monthcard_val,
            $yearcard_val,
            //$ccv_val,
            $postalcode_val,
            $address_val,
            $city_val,
            $prov_val,
            $specialrequest_val,
            $reserv_hash);

    $ret = $wpdb->query($sql);


    // get the new reserv object

    $reserv = get_reserv($reserv_hash);
  
    $ret = make_payment($reserv,
            $ccv_val);
    if($ret['ACK'] == "SUCCESS")
    {
      $sql = $wpdb->prepare("UPDATE `wp_reserv` SET
									`state` ='Payed',
                  WHERE md5(CONCAT(id,'__dtdtdt'))=%s LIMIT 1",$reserv_hash);

      wp_mail($current_user->user_email,
              'Your dinner thrill reservation',
              'Thank you for your reservation at ' . $post->name . "          <ul>" .
              "<li>" . $reserv->personnes . " Ppl. </li>" .
              "<li>" . substr($reserv->time,
                      0,
                      -3) . "</li>" .
              "<li>" . date('F j',
                      strtotime($reserv->date)) . "</li>" .
              "</ul>");
      ob_start();
      require_once(get_theme_root() . "/" . get_current_theme() . "/_step3.php");
      $response = ob_get_clean();

      // Finally mark reservation as payed
      $ret = $wpdb->query($sql);
    }
    else
    {
      ob_start();
      require_once(get_theme_root() . "/" . get_current_theme() . "/_step_error.php");
      $response = ob_get_clean();
    }
  }
  else
    $response = "Invalid post data:" . print_r($_POST,
                    true);

  header("Content-Type: text/html");
  echo $response;
  exit;
}

function ajax_notlogged()
{
  $response = json_encode(array('success' => false, 'message' => 'You are not logged in'));
  header("Content-Type: application/json");
  echo $response;
  exit;
}

register_activation_hook(__FILE__,
        'reserv_install');

define("PLUGIN_DIR",
        basename(dirname(__FILE__)));
define("PLUGIN_URL",
        get_settings("siteurl") . "/wp-content/plugins/" . PLUGIN_DIR);

function reserv_menu()
{
  global $submenu, $menu, $wpdb;

  add_menu_page(__("Reservations",
                  "wp-reservation"),
          __("Reservations",
                  "wp-reservation"),
          0,
          "reservlist",
          'reserv_list'
          ,
          PLUGIN_URL . "/img/go.png");

}

if(!class_exists('WP_List_Table'))
{
  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Reserv_List_Table extends WP_List_Table
{

  function __construct()
  {
    global $status, $page;

    //Set parent defaults
    parent::__construct(array(
            'singular' => 'reservation', //singular name of the listed records
            'plural' => 'reservations', //plural name of the listed records
            'ajax' => false        //does this table support ajax?
    ));
  }

  function column_default($item, $column_name)
  {
    switch($column_name)
    {
      case 'state_':
      case 'when_':
      case 'where_':
      case 'who_':
        return $item[$column_name];
      default:
        return print_r($item,
                        true); //Show the whole array for troubleshooting purposes
    }
  }

  /*    function column_title($item){

    //Build row actions
    $actions = array(
    'edit'      => sprintf('<a href="?page=%s&action=%s&movie=%s">Edit</a>',$_REQUEST['page'],'edit',$item['ID']),
    'delete'    => sprintf('<a href="?page=%s&action=%s&movie=%s">Delete</a>',$_REQUEST['page'],'delete',$item['ID']),
    );

    //Return the title contents
    return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
    /*$1%s */ /* $item['rest_name'],
    /*$2%s */ /* $item['ID'],
    /*$3%s */ /* $this->row_actions($actions)
    );
    }
   */

  function column_cb($item)
  {
    return sprintf(
                    '<input type="checkbox" name="%1$s[]" value="%2$s" />',
                    /* $1%s */
                    $this->_args['singular'], //Let's simply repurpose the table's singular label ("movie")
                    /* $2%s */
                    $item['ID']                //The value of the checkbox should be the record's id
    );
  }

  function get_columns()
  {
    $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            'state_' => 'State',
            'when_' => 'When',
            'where_' => 'Where',
            'who_' => 'Who'
    );
    return $columns;
  }

  function get_sortable_columns()
  {
    $sortable_columns = array(
            //'title'     => array('title',true),     //true means its already sorted
            //'rating'    => array('rating',false),
            //'director'  => array('director',false)
    );
    return $sortable_columns;
  }

  function get_bulk_actions()
  {
    $actions = array(
            'delete' => 'Delete'
    );
    return $actions;
  }

  function process_bulk_action()
  {

    //Detect when a bulk action is being triggered…
    if('delete' === $this->current_action())
    {
      wp_die('Items deleted (or they would be if we had items to delete)!');
    }
  }

  function prepare_items()
  {
    global $wpdb;
    $per_page = 50;

    $columns = $this->get_columns();
    $hidden = array();
    $sortable = $this->get_sortable_columns();

    $this->_column_headers = array($columns, $hidden, $sortable);

    $this->process_bulk_action();

    $qry = "SELECT 
r.ID,
r.state as state_,
p.post_name as where_,
CONCAT(r.date,' ',r.time)  as when_,
CONCAT(r.firstname,' ',r.lastname,IF(r.phone,CONCAT(' (',r.phone,')'),'')) as who_ 
FROM wp_reserv  r
LEFT JOIN wp_posts p ON (r.resto_id = p.ID)
LEFT JOIN wp_users u ON (r.user_id = u.ID)
WHERE 1";

    $data = $wpdb->get_results($wpdb->prepare($qry),
            ARRAY_A);


    $current_page = $this->get_pagenum();
    $total_items = count($data);
    $data = array_slice($data,
            (($current_page - 1) * $per_page),
            $per_page);
    $this->items = $data;
    $this->set_pagination_args(array(
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page' => $per_page, //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items / $per_page)   //WE have to calculate the total number of pages
    ));
  }

}

function reserv_list()
{

  $listTable = new Reserv_List_Table();
  $listTable->prepare_items();
  ?>
  <div class="wrap">

    <div id="icon-users" class="icon32"><br/></div>
    <h2>Reservations</h2>


    <form id="reserv-filter" method="get">
      <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
  <?php $listTable->display() ?>
    </form>

  </div>
  <?php
}

function reserv_install()
{
  global $wpdb;
  $sql = "
CREATE TABLE `wp_reserv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  `resto_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `personnes` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `due` float NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `MM` varchar(2) NOT NULL,
  `YY` varchar(2) NOT NULL,
  `ccnum` varchar(20) NOT NULL,
  `billing_pc` varchar(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `prov` varchar(2) NOT NULL,
  `special` text NOT NULL,
  `savecard` tinyint(4) NOT NULL,
   PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

";

  $wpdb->query($sql);
}

function reserv()
{
  
}
?>
