<?php

define('API_USERNAME',
        'jkeabl_1340505137_biz_api1.gmail.com');
define('API_PASSWORD',
        '1340505170');
define('API_SIGNATURE',
        'A5VTaskSqNS8CwtsMpf0z77ZXXqEAtYQBnsgUYbvoUKxcDsm8xPILiWB');
define('API_ENDPOINT',
        'https://api-3t.sandbox.paypal.com/nvp');
define('SUBJECT',
        '');
define('USE_PROXY',
        FALSE);
define('PAYPAL_URL',
        'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=');
define('VERSION',
        '87.0');
define('ACK_SUCCESS',
        'SUCCESS');
define('ACK_SUCCESS_WITH_WARNING',
        'SUCCESSWITHWARNING');

require_once(DIRNAME(__FILE__) . "/CallerService.php");

function make_payment($r,$cvv)
{
  $paymentType = "Sale";
  $firstName = urlencode($r->firstname);
  $lastName = urlencode($r->lastname);
  $creditCardType = "Visa";
  $creditCardNumber = urlencode($r->ccnum);
  $expDateMonth = urlencode($r->MM);

// Month must be padded with leading zero
  $padDateMonth = str_pad($expDateMonth,
          2,
          '0',
          STR_PAD_LEFT);

  $expDateYear = urlencode("20".$r->YY);
  $cvv2Number = urlencode($cvv);
  $address1 = urlencode("9123 rue blabal");
  $address2 = urlencode("");
  $city = urlencode("Montreal");
  $state = urlencode("QC");
  $zip = urlencode("H2H 1K3");
  $amount = urlencode("10.0");
//$currencyCode=urlencode($_POST['currency']);
  $currencyCode = "CAD";

  $nvpstr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=" . $padDateMonth . $expDateYear . "&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state" .
          "&ZIP=$zip&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";

  $resArray = hash_call("doDirectPayment",
          $nvpstr);

  $resArray["ACK"] = strtoupper($resArray["ACK"]);
  return $resArray;
}