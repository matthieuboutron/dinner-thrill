<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="UTF-8" />
<title>Detail | Dinner Thrill</title>
<link rel="stylesheet" href="http://localhost/dinner-thrill/wp-content/plugins/sitepress-multilingual-cms/res/css/language-selector.css?v=2.4.3" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" media="all" href="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/bootstrap.css" />
<meta name='robots' content='noindex,nofollow' />
<link rel="alternate" type="application/rss+xml" title="Dinner Thrill &raquo; Contact Us Comments Feed" href="http://localhost/dinner-thrill/contact-us/feed/" />
<link rel='stylesheet' id='admin-bar-css'  href='http://localhost/dinner-thrill/wp-includes/css/admin-bar.css?ver=20111209' type='text/css' media='all' />
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js?ver=3.3.1'></script>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js?ver=3.3.1'></script>
<script type='text/javascript' src='http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/js/slides.min.jquery.js?ver=3.3.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/dinner-thrill/xmlrpc.php?rsd" />
<script type="text/javascript">var icl_lang = 'en';var icl_home = 'http://localhost/dinner-thrill/';</script>
<script type="text/javascript" src="http://localhost/dinner-thrill/wp-content/plugins/sitepress-multilingual-cms/res/js/sitepress.js"></script>
<meta name="generator" content="WPML ver:2.4.3 stt:1,4;0" />
<style type="text/css" media="print">
#wpadminbar {
	display:none;
}
</style>
<style type="text/css" media="screen">
html {
	margin-top: 28px !important;
}
* html body {
	margin-top: 28px !important;
}
</style>
</head>
<body>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : 'YOUR_APP_ID',
      status     : true, 
      cookie     : true,
      xfbml      : true,
      oauth      : true,
    });
  };
  (function(d){
     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     d.getElementsByTagName('head')[0].appendChild(js);
   }(document));
</script>
<div id="main-container">
  <div id="header-container">
    <div id="header">
      <div id="header-content">
        <div id="header-menu">
          <ul>
            <li class="invite"><a href="#_" class="yellowarrow">Invite friends and get <strong>$10</strong></a></li>
            <li> <a href="#_">My Account</a> </li>
            <li>|</li>
            <li> <a href="#_">Sign Out</a> </li>
          </ul>
        </div>
        <a id="header-logo" href="http://localhost/dinner-thrill" title=""> <img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/dinnerthrill-logo.png" alt="Dinner Thrill - to Homepage" /> </a> <img id="header-logo-backdrop" src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/logo-backdrop.png" alt="" />
        <div id="header-credit">
          <ul>
            <li><a href="#_" class="yellowarrow">Back to search</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- #header -->
  
  <div id="main-pane" class="template_detail">
    <div id="highlights" style="background:url(http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/temp-highlights-results-photo.jpg) no-repeat">
      <div class="bandeau">
        <h1>Big in japan</h1>
      </div>
      <div id="mod-reservation">
        <form action="" method="get">
          <table width="235" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="235" height="80" colspan="2" class="valign-t"><h2>Make a<br>
                  <strong>reservation</strong></h2>
                <p><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/sep-reserv1.png" width="235" height="15" alt=" "></p>
                <table width="235" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="perceL"><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/30percent.jpg" width="73" height="32"></td>
                    <td class="perceR">off all <br/>
                      food & drink</td>
                  </tr>
                </table>
                <p><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/sep-reserv2.png" width="235" height="15" alt=" "></p></td>
            </tr>
          </table>
          <div class="step1" style="display:none">
            <table width="235" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="65" class="valign-m"><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/traduction/en/for.png" width="46" height="45" alt="for"></td>
                <td width="170"><div class="field-wrap people-field">
                    <div class="dropdown-selector"></div>
                    <label for="message_subject">People</label>
                    <input id="message_subject" type="hidden" value="null" name="message_subject">
                    <div class="error-sign"></div>
                  </div></td>
              </tr>
              <tr>
                <td width="65">&nbsp;</td>
                <td width="170"><ul class="people">
                    <li><a href="#_">1 Person</a></li>
                    <li><a href="#_"> 2 People</a></li>
                    <li><a href="#_"> 3 People</a></li>
                    <li><a href="#_"> 4 People</a></li>
                    <li><a href="#_"> 5 People</a></li>
                    <li><a href="#_"> 6 People</a></li>
                    <li><a href="#_"> 7 People</a></li>
                    <li><a href="#_"> 8 People</a></li>
                  </ul></td>
              </tr>
              <tr>
                <td width="65" class="valign-m"><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/traduction/en/on.png" width="46" height="45" alt="on"></td>
                <td width="170"><div class="field-wrap date-field">
                    <div class="dropdown-selector" data-choices=""></div>
                    <label for="select_date">DD/MM/YY</label>
                    <input id="select_date" type="hidden" value="null" name="select_date">
                    <div class="error-sign"></div>
                  </div></td>
              </tr>
              <tr>
                <td width="65" class="valign-m"><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/traduction/en/for.png" width="46" height="45" alt="for"></td>
                <td width="170"><div class="field-wrap meals-field">
                    <div class="dropdown-selector"></div>
                    <label for="select_meals">Lunch</label>
                    <input id="select_meals" type="hidden" value="null" name="select_meals">
                    <div class="error-sign"></div>
                  </div></td>
              </tr>
              <tr>
                <td width="235" colspan="2" ><ul class="time">
                    <li><a href="#_">11:30 AM</a></li>
                    <li><a href="#_" class="selected">12:00 AM</a></li>
                    <li><a href="#_">12:30 AM</a></li>
                    <li><a href="#_">13:00 AM</a></li>
                    <li><a href="#_">13:30 AM</a></li>
                  </ul>
                  <p class="time">Available  | <em>Selected</em></p></td>
              </tr>
              <tr>
                <td width="235" class="valign-b" colspan="2" height="60"><a href="#_" class="next_step">Continue</a></td>
              </tr>
            </table>
          </div>
          <div class="step2">
          <table width="235" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="border_white"><ul>
      <li>2 Ppl. </li>
      <li>2:30PM</li>
      <li>June 9</li>
    </ul>      </td>
    <td class="txt-r border_white"><a href="#_" class="yellowarrow">Edit</a></td>
  </tr>
  <tr>
    <td class="valign-m"><p class="big padT25">Amount Due </p></td>
    <td class="txt-r"><p class="big yellow padT25">$10</p></td>
  </tr>
  <tr>
    <td colspan="2">
      <img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/sep-reserv1.png" width="235" height="15" alt=" " class="valign-t"> <br>	
      <p class="big">reservation info</p>
      
      
      
  </td>
    </tr>
  <tr>
    <td colspan="2">
    <div class="field-wrap user-name-field">
	            <input type="text" id="first_name" name="first_name" maxlength="200" class="mandatory-field" value="" />
	            <label for="first_name">First name</label>
	            <div class="error-settings"></div></div></td>
    </tr>
  <tr>
    <td colspan="2">
    <div class="field-wrap user-name-field">
	            <input type="text" id="last_name" name="last_name" maxlength="200" class="mandatory-field" value="" />
	            <label for="last_name">Last name</label>
	            <div class="error-settings"></div></div></td>
    </tr>
  <tr>
    <td colspan="2">
    <div class="field-wrap user-name-field">
	            <input type="text" id="phone_number" name="phone_number" maxlength="200" class="mandatory-field" value="" />
	            <label for="phone_number">Phone number</label>
	            <div class="error-settings"></div></div></td>
    </tr>
  <tr>
    <td colspan="2">
    <div class="field-wrap user-name-field">
	            <input type="text" id="creditcard_number" name="creditcard_number" maxlength="200" class="mandatory-field" value="" />
	            <label for="creditcard_number">Credit card number</label>
	            <div class="error-settings"></div></div></td>
    </tr>
  <tr>
    <td colspan="2">
      <table width="235" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="60"><div class="field-wrap month-field" style="width:50px;">
                          <input type="text" id="month_card" name="month_card" class="mandatory-field" />
                          <label for="month_card">MM</label>
                          <div class="dropdown-selector" data-choices="01,02,03,04,05,06,07,08,09,10,11,12"></div>
                          <div class="error-sign"></div>
                        </div></td>
                      <td width="22">&nbsp;</td>
                      <td width="60"><div class="field-wrap year-field" style="width:50px;">
                          <input type="text" id="year_card" name="year_card" class="mandatory-field" />
                          <label for="year_card">YY</label>
                          <div class="dropdown-selector" data-choices="12,13,14,15,16,17,18"></div>
                          <div class="error-sign"></div>
                        </div></td>
                      <td width="22">&nbsp;</td>
                      <td width="71"><div class="field-wrap ccv-field" style="width:61px;">
                          <input type="text" id="ccv" name="ccv" maxlength="200" class="mandatory-field" value="" />
                          <label for="creditcard_number">CCV</label>
                          <div class="error-settings"></div>
                        </div></td>
                    </tr>
                  </table>
    
    
    </td>
    </tr> <tr>
    <td colspan="2">
    <div class="field-wrap user-name-field">
	            <input type="text" id="address" name="address" maxlength="200" class="mandatory-field" value="" />
	            <label for="address">Billing address</label>
	            <div class="error-settings"></div></div></td>
    </tr>
  <tr>
    <td colspan="2">
    <div class="field-wrap user-name-field">
	            <input type="text" id="postal_code" name="postal_code" maxlength="200" class="mandatory-field" value="" />
	            <label for="postal_code">Billing postal code</label>
	            <div class="error-settings"></div></div></td>
    </tr>
  <tr>
    <td colspan="2">  <p><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/sep-reserv1.png" width="235" height="15" alt=" "></p></td>
    </tr>
  <tr>
    <td><p class="big no_marg">Special request?</p></td>
    <td class="txt-r"><p class="no_marg"><a href="#_" class="yellowarrow">Add</a></p></td>
  </tr>
  <tr>
    <td colspan="2">  <p><img src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/sep-reserv1.png" width="235" height="15" alt=" "></p></td>
  </tr>
       <td width="235" class="valign-b" colspan="2" height="60"><p class="padT25"><a href="#_" class="next_step">Reserve</a></p>
         <p class="big padT15">Read the terms</p>
         <ul class="readterms">
           <li>Cannot be combined with any other deal 
             or promotion.</li>
           <li> An 18% gratuity will be added to the 
             pre-discounted bill.</li>
         </ul>         </td>
  </tr>
</table>
          
           </div>
        </form>
      </div>
    </div>
    <div id="sidebar">
      <div class="fiche">
        <div class="neigh">
          <div>
            <h4>Neighborhood</h4>
            <p>Plateau</p>
          </div>
        </div>
        <div class="cuisine">
          <div>
            <h4>Cuisine</h4>
            <p>Japanese</p>
          </div>
        </div>
        <div class="scene">
          <div>
            <h4>Scene</h4>
            <p>Cozy &amp; Casual<br>
              Gather The Group<br>
              Take Away</p>
          </div>
        </div>
        <div class="cost">
          <div>
            <h4>Cost</h4>
            <p>$$</p>
          </div>
        </div>
      </div>
      <h3>Details</h3>
      <p>1234 rue Saint-Laurent, <br>
        Montreal, H2H 1P4</p>
      <p>Telephone: 514.234.1331</p>
      <ul>
        <li><a href="#_">Directions</a></li>
        <li><a href="#_">Menu</a></li>
        <li><a href="#_">Website</a></li>
      </ul>
      <div class="map">
        <iframe width="275" height="195" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=55+rue+mont-royal+ouest,+Montreal&amp;aq=&amp;sll=45.536047,-73.571618&amp;sspn=0.011934,0.01972&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=55+Avenue+du+Mont-Royal+Ouest,+Montr%C3%A9al,+Communaut%C3%A9-Urbaine-de-Montr%C3%A9al,+Qu%C3%A9bec+H2T+1N9&amp;ll=45.519459,-73.587027&amp;spn=0.011727,0.023603&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
      </div>
    </div>
    <div id="detail-content">
      <h2> Why We Love It</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas rutrum suscipit ipsum, non fringilla est congue consequat. Vestibulum auctor mollis sem, egestas adipiscing est varius et. In pellentesque feugiat adipiscing. Integer et neque est. Vivamus augue erat, aliquet sodales luctus vel, rutrum at leo. </p>
      <p>Vestibulum ligula ante, imperdiet in euismod at, accumsan ut erat. Vestibulum pulvinar augue vel turpis varius eleifend. Cras et ipsum sem. Nam adipiscing sapien quis nulla imperdiet eu accumsan ligula blandit. Nulla facilisi. Maecenas vestibulum accumsan metus, at ultrices urna viverra ut. </p>
      <p>Pellentesque feugiat interdum mi, eget vestibulum orci cursus vel. Phasellus mollis, augue eget venenatis vulputate, odio metus sagittis eros, non euismod purus quam sit amet sem. Sed sapien leo, dictum nec tempor ut, malesuada id massa.</p>
      <h2>Press &amp; Reviews</h2>
      <h3>Montreal Reader</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas rutrum suscipit ipsum, non fringilla est congue consequat. Vestibulum auctor mollis sem, egestas adipiscing est varius et. In pellentesque feugiat adipiscing. Integer et neque est. Vivamus augue erat, aliquet sodales luctus vel, rutrum at leo. Vestibulum ligula ante, imperdiet in euismod at, accumsan ut erat. Vestibulum pulvinar augue vel turpis varius eleifend. </p>
      <h3>Daily Candy</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas rutrum suscipit ipsum, non fringilla est congue consequat. Vestibulum auctor mollis sem, egestas adipiscing est varius et. In pellentesque feugiat adipiscing. Integer et neque est. Vivamus augue erat, aliquet sodales luctus vel, rutrum at leo. Vestibulum ligula ante, imperdiet in euismod at, accumsan ut erat. Vestibulum pulvinar augue vel turpis varius eleifend. </p>
    </div>
    <div class="clear-it"></div>
  </div>
  <!-- #main-pane -->
  <div id="footer-callaction">
    <div> <a href="#_"> Invite friends and get <strong>$10</strong> </a></div>
  </div>
  <div id="footer-container">
    <div id="footer">
      <div id="dinnerthrill-copyright">© 2012 Dinner Thrill </div>
      <div id="legal">
        <ul>
          <li><a href="#_">Terms of use </a></li>
          <li>|</li>
          <li><a href="#_">Privacy Policy</a></li>
        </ul>
      </div>
      <div id="footer-menu">
        <ul id="menu-footer" class="menu">
          <li><a href="http://localhost/dinner-thrill/about-us/">About Us</a></li>
          <li><a href="http://localhost/dinner-thrill/contact-us/">Contact Us</a></li>
          <li><a href="http://localhost/dinner-thrill/faq/">FAQ</a></li>
        </ul>
      </div>
      <div id="footer-social">
        <div class="social-title">Follow us</div>
        <div class="social-facebook"><a href="" title="on Facebook">on Facebook</a></div>
        <div class="social-twitter"><a href="" title="on Twitter"></a></div>
      </div>
    </div>
  </div>
  <div class="dropdown-backdrop"></div>
</div>
<!-- #main-container -->

<div class="background-element garlic-left"><img class="image" src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/wallpapers/garlic-left.jpg" /></div>
<div class="background-element garlic-right"><img class="image" src="http://localhost/dinner-thrill/wp-content/themes/dinnerthrill/img/wallpapers/garlic-right.jpg" /></div>
<script type="text/javascript">

function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
  return pattern.test(emailAddress);
};

$(function() {

  // TODO ScrollSpy that changes the menu upon scrolling
  // $(window).scroll(function() {
  //   var position = $(this).scrollTop();
  //   console.log(position);
  //   if(position > 56) {
  //     $('#header-container').addClass('fixed');
  //   } else {
  //     $('#header-container').removeClass('fixed');
  //   }
  // });
 

  
  
  
  
    // Initializes label placeholders
  $('form .step2 label').each(function() {
    var value = $(this).parent().find('input').val();
    if(value != '' && $(this).parent().hasClass('subject-field') != true) {
      $(this).animate({
      'left': '240px',
      'opacity': '0.3' }, 300, 'easeOutBack');
    }
  });
  $('form .step2 label').each(function() {
    var value = $(this).parent().find('textarea').val();
    if(value != '') {
      $(this).animate({
      'top': '10px',
      'opacity': '0.3' }, 300, 'easeOutBack');
    }
  });
  // Hides label placeholder on focus
  $('form .step2 input').focus(function() {
    $(this).parent().find('label').animate({
      'left': '240px',
      'opacity': '0.3' }, 300, 'easeOutBack');
  });
  // Show label placeholder on blur
  $('form .step2 input').blur(function() {
    if($(this).val() == '') {
      $(this).parent().find('label').animate({
        'left': '5px',
        'opacity': '1' }, 300, 'easeInBack');
    }
  });
  // Hides label placeholder on focus
  $('form .step2 textarea').focus(function() {
    $(this).parent().find('label').animate({
      'top': '-20px',
      'opacity': '0.3' }, 300, 'easeOutBack');
  });
  // Show label placeholder on blur
  $('form .step2 textarea').blur(function() {
    if($(this).val() == '') {
      $(this).parent().find('label').animate({
        'top': '0',
      'opacity': '1' }, 300, 'easeInBack');
    }
  });

  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .subject-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });
  $('.dropdown-backdrop').click(function() {
    $(this).fadeOut(300, 'easeOutCubic');
    $('.dropdown-choices').fadeOut(300, 'easeOutCubic');
  })

  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .people-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });
  $('.dropdown-backdrop').click(function() {
    $(this).fadeOut(300, 'easeOutCubic');
    $('.dropdown-choices').fadeOut(300, 'easeOutCubic');
  })

  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .meals-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });

  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .date-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });


  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .neighborhood-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });


  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .cuisine-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });


  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .scene-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });


  // Displays / Hides the dropdown choices
  $('.dropdown-selector, .cost-field label').click(function() {
    if($('.dropdown-choices').length == 0) {
      $('#main-container').append('<div class="dropdown-choices"><ul class="dropdown-choices-list"></ul></div>');
    }
    var position = $(this).parent().offset();
    var item_height = $(this).parent().height() - 18;
    var item_width = $(this).parent().width();
    if($(this).is('.dropdown-selector') == true)
      var choices = $(this).attr('data-choices');
    else
      var choices = $(this).parent().find('.dropdown-selector').attr('data-choices');
    choices = choices.split(',');
    
    $('.dropdown-choices').css('top', position.top + item_height + 'px').css('left', position.left - (($('.dropdown-choices').width() - item_width) / 2) + 'px');
    var choice;
    $('.dropdown-choices-list').html('');
    for (choice in choices) {
      $('.dropdown-choices-list').append('<li>'+choices[choice]+'</li>');
    }
    $('.dropdown-backdrop').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').fadeIn(300, 'easeOutCubic');
    $('.dropdown-choices').find('li').click(function(e) {
      var the_choice = $(this).html();
      $('.dropdown-selector').parent().find('input').val(the_choice);
      $('.dropdown-selector').parent().find('label').html(the_choice);
      $('.dropdown-backdrop').click();
    });
  });








  // Creates the user account on submit
  $('#contact-us').submit(function(e) {
    e.preventDefault();
    var error = 0;
    var error_code = {
      is_missing_field    : false,
      is_blacklisted      : false,
      is_invalid_email    : false
    }
    var user_name = $('#user_name').val();
    var user_email = $('#user_email').val();
    var message_subject = $('#message_subject').val();
    var message_content = $('#message_content').val();
    var user_lang = $('#user_lang').val();
    var user_ip = $('#user_ip').val();
    //var user_lang = $('#cc_user_lang').val();

    // Removes error messages (from previous attempts)
    $('.is-error-code').slideUp(200, 'easeOutCubic');

    $('.mandatory-field').each(function() {
      if($(this).val() == '') {
        $(this).parent().addClass('has_error');
        $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
        error++;
        error_code.is_missing_field = true;
      } else {
        if($(this).is('#user_email') && !isValidEmailAddress(user_email)) {
          $(this).parent().addClass('has_error');
          $(this).parent().find('.error-sign').show('slide', {direction: 'right'}, 300);
          error++;
          error_code.is_invalid_email = true;
        } else {
          $(this).parent().find('.error-sign').fadeOut(200, 'easeOutCubic');
          $(this).parent().removeClass('has_error');         
        }
      }
    }); 
    
    if(error > 0) {
      if(error_code.is_invalid_email) $('.is-invalid-email').slideDown(500, 'easeOutCubic');
      if(error_code.is_missing_field) $('.is-missing-field').slideDown(500, 'easeOutCubic');
      return false;
    } else {
      var data = {
        action: 'dt_contact_us',
        user_name: user_name,
        user_email: user_email,
        message_subject: message_subject,
        message_content: message_content,
        user_lang: user_lang,
        user_ip: user_ip
      };
      $('#submit_invite').hide('slide', {direction: 'down'}, 300);
      $('#ajaxsave').show('slide', {direction: 'up'}, 300);
      $.post("http://localhost/dinner-thrill/wp-admin/admin-ajax.php", data,
      function(response){
        $('#ajaxsave').hide('slide', {direction: 'down'}, 300);
        $('#submit_invite').show('slide', {direction: 'up'}, 300);
        $('.mandatory-field').removeClass('has_error');
        if(response == 'is_failure') 
          $('.is-failure').slideDown(500, 'easeOutCubic');
        if(response == 'is_ok') {
          $('.mandatory-field').val('');
          $('.is-success').slideDown(500, 'easeOutCubic');
        }
          
      });   
      return false;
    }
  });

  



  $('.image').load(function (e) {
    $(this).parent().fadeIn(800);
  });

});
</script>
</body>
</html>
